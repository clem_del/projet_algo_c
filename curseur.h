#ifndef CURSEUR_H
#define CURSEUR_H
#include "regles.h"

void deplacer_curseur(int pecheurX,int pecheurY,int *pointeurX,int *pointeurY, direction direction_deplacement_curseur,int distance_max_curseur,animal ***creature);//permet de deplacer le curseur dans la direction donnee

#endif // CURSEUR_H
