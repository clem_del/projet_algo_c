#include "curseur.h"


int est_assez_proche(int pointeurX,int pointeurY,int joueurX,int joueurY,int distance_max)//retourne VRAI si le pointeur est a moins de distance_max du joueur, FAUX autrement
{
    if((pointeurX!=joueurX || pointeurY!=joueurY) && (pointeurX-joueurX)<=distance_max && (pointeurX-joueurX)>=(0-distance_max) && (pointeurY-joueurY)<=distance_max && (pointeurY-joueurY)>=(0-distance_max))
    {
        return VRAI;
    }else
    {
        return FAUX;
    }
}

void deplacer_curseur(int pecheurX,int pecheurY,int *pointeurX,int *pointeurY, direction direction_deplacement_curseur,int distance_max_curseur,animal ***creature)//gere le deplacement du curseur selon la direction donnee
{
    int ancienX=*pointeurX;//on sauvegarde les coordonnees du pointeur
    int ancienY=*pointeurY;
    switch(direction_deplacement_curseur){//suivant la direction choisie
    case DROITE://si c'est droite
        *pointeurY+=1;
        break;
    case BAS://si c'est bas
        *pointeurX+=1;
        break;
    case GAUCHE://si c'est gauche
        *pointeurY-=1;
        break;
    case HAUT://si c'est haut
        *pointeurX-=1;
        break;
    default://en cas d'erreur
        *pointeurX=ancienX;//on prend les valeurs de depart des coordonnees
        *pointeurY=ancienY;
        return;
    }
    if(est_dans_carte(*pointeurX,*pointeurY) && est_assez_proche(*pointeurX,*pointeurY,pecheurX,pecheurY,distance_max_curseur))//si le pointeur est dans la carte et est a la bonne distance
    {
        creature[ancienX][ancienY]->selection=0;//on deselectionne la case qui etait sous le curseur avant
        creature[*pointeurX][*pointeurY]->selection=1;//on selectionne la nouvelle
        creature[pecheurX][pecheurY]->selection=2;//on marque le pecheur
    }else//si le mouvement donné par le joueur a rendu le pointeur dans un endroit inaccessible
    {
        *pointeurX=ancienX;//on prend les valeurs de depart des coordonnees
        *pointeurY=ancienY;
    }
}
