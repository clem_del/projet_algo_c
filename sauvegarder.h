#ifndef SAUVEGARDER_H
#define SAUVEGARDER_H
#include <string.h>
#include <time.h>
#include "regles.h"


//fonctions pour l'enregistrement des statistiques de la partie
char *nomFichier;

void ajouterEntreeTour(void);//permet de rajouter une entree dans le fichier de statistiques pour le tour courant

void ajouterEnTete(void);//permet d'ajouter l'en-tete du fichier de statistiques


//fonctions pour la sauvegarde et la restauration de cartes et de parties
void enregistrer_carte(const char *nom_fichier, animal *** creature);//permet d'enregistrer la carte en cours

void enregistrer_partie_en_cours(const char *nom_fichier, int numero_tour, pecheur *joueurs, int nbjoueurs, animal *** creature);//permet de sauvegarder la partie en cours

int chargement_carte(const char *nom_fichier, animal ***creature);//permet de charger une carte sauvegardee

int lire_partie_sauvegardee(const char *nom_fichier, int *numero_tour, pecheur *joueurs, animal ***creature);//permet de lire une partie sauvegardee

int charger_configuration_animaux(void);//charge les constantes depuis un fichier de configuration

#endif // SAUVEGARDER_H
