#include "sauvegarder.h"


void ajouterEntreeTour(void){//permet d'enregistrer dans un fichier le nombre de creatures de chaque espece presentes sur la carte a un moment donne
    FILE *fichier = fopen(nomFichier,"a");//on ouvre le fichier


    fprintf(fichier, "%i;%i;%i;%i;%i;%i;%i;%i;%i;%i\n",
            tour_courant,
            bestiaire[PLANCTON].nombre_total,
            bestiaire[CORAIL].nombre_total,
            bestiaire[BAR].nombre_total,
            bestiaire[THON].nombre_total,
            bestiaire[POLLUTION].nombre_total,
            bestiaire[PIRHANNA].nombre_total,
            bestiaire[REQUIN].nombre_total,
            bestiaire[ORQUE].nombre_total,
            bestiaire[BALEINE].nombre_total);//on enregistre le nombre de creatures de chaque espece presentes sur la carte lors de ce tour

    fclose(fichier);//on ferme le fichier
}

void ajouterEnTete(void){//permet d'ajouter l'en-tete du fichier de statistiques
	nomFichier = calloc(120,sizeof(char)); 
	char *temp = calloc(30,sizeof(char)); 
	time_t timestamp; 
  
    timestamp = time(NULL);

    strcpy(nomFichier, "archive-sauvegarde-n-");//creation nom de fichier
	sprintf(temp,"%d",(int)timestamp);  
	strcat(nomFichier,temp);
	strcat(nomFichier,".csv"); 
    FILE *fichier = fopen(nomFichier,"w");//on cree/ouvre le fichier


    fprintf(fichier, "Tour;PLANCTON;CORAIL;BAR;THON;POLLUTION;PIRHANNA;REQUIN;ORQUE;BALEINE\n");//on inscrit l'en-tete
    fclose(fichier);//on ferme le fichier
    free(temp);
}

void enregistrer_carte(const char *nom_fichier, animal *** creature){//permet d'enregistrer une carte dans un fichier
  FILE *fichier = fopen(nom_fichier,"w");//on ouvre le fichier

  if(fichier != NULL){//si le fichier s'est bien ouvert
    int i,j;
    fprintf(fichier,"%i\n",M);
    for(j = 0; j < M; j++){//on parcourt la carte
      for(i = 0; i < M; i++){
        fprintf(fichier, "%i;%i;%i\n",i,j,creature[i][j]->type_animal);//on inscrit a chaque fois dans le fichier les coordonnees puis le type d'animal present
      }
    }
  }
  else {//si on ne peut pas ouvrir le fichier
    printf("Impossible d'écrire dans le fichier %s.\n",nom_fichier);
    return;//on sort de la fonction
  }

  fclose(fichier);//on ferme le fichier
}

int chargement_carte(const char *nomFichier, animal ***creature){//permet de charger une carte sauvegardee
  FILE *fichier = fopen(nomFichier,"r");//on ouvre le fichier

  if(fichier != NULL){//si l'ouverture s'est bien deroulee
    int i,j,type,taille;
    fscanf(fichier,"%i\n",&taille);
    while(fscanf(fichier, "%i;%i;%i\n",&i,&j,&type) != EOF){//tant qu'on est pas a la fin du fichier on lit
        if(i<M && j<M)//si les coordonnees lues sont dans la carte
        {
            creer_animal(creature[i][j],type);//on cree un animal de la bonne espece
        }else//sinon
        {
            printf("\nOversized map !\n");
            return FAUX;//la map est trop grande
        }
    }
  }
  else {//si l'on a pas pu ouvrir le fichier
    printf("Impossible de lire le fichier %s.\n",nomFichier);
    return FAUX;
  }

  fclose(fichier);//on ferme le fichier
  return VRAI;//chargement reussi
}

int charger_configuration_animaux(void){//permet de charger la configuration de l'ecosysteme
  FILE *fichier = fopen("configuration.csv", "r");//on ouvre le fichier

  if(fichier != NULL){//si l'ouverture s'est bien passe
    int i = 0;
    int type;
    while(fscanf(fichier,"%i;",&type) != EOF){//tant qu'on arrive pas au bout du fichier
      if(type){//si c'est une ligne de donnees
        char flag[4];
        int satiete_initiale, duree_de_survie, taille, taille_du_bide,
          saut_max, metabolisme, gestation, frequence_reproduction, taux_apparition;

        fscanf(fichier, "%3s;%i;%i;%i;%i;%i;%i;%i;%i;%i;\n",flag,//on lit une ligne du fichier de config
                    &satiete_initiale,
                    &duree_de_survie,
                    &taille,
                    &taille_du_bide,
                    &saut_max,
                    &metabolisme,
                    &gestation,
                    &frequence_reproduction,
                    &taux_apparition);

        bestiaire[i].nombre_total = 0;//on affecte les differentes valeurs lues au bestiaire
        bestiaire[i].dernier_repas = 0;
        bestiaire[i].satiete = satiete_initiale;
        bestiaire[i].derniere_reproduction = 0;
        bestiaire[i].duree_survie =  duree_de_survie;
        bestiaire[i].taille = taille;
        bestiaire[i].taille_du_bide = taille_du_bide;
        bestiaire[i].saut_max = saut_max;
        bestiaire[i].metabolisme = metabolisme;
        bestiaire[i].gestation = gestation;
        bestiaire[i].frequence_reproduction = frequence_reproduction;
        bestiaire[i].taux_apparition = (int)(((float) taux_apparition/100)*((M-2)*(M-2)));

        i++;//on passe a l'espece suivante
      }
      else {//si ce n'est pas une ligne de donnees
        // Lecture d'une ligne d'en-tete
        char buffer[1024];
        fscanf(fichier,"%s\n",buffer);//on lit la ligne
      }
    }
  }
  else {//si l'on ne peut pas lire le fichier de config
    printf("Impossible de lire le fichier de configuration.\n");
    printf("Utilisation des variables integrees...\n");
    creer_bestiaire();//on utilise les variables integrees
    return -1;
  }

  fclose(fichier);//on ferme le fichier

  return 0;
}

void enregistrer_partie_en_cours(const char *nom_fichier, int numero_tour, pecheur *joueurs, int nbjoueurs, animal *** creature)//permet d'enregistrer la partie en cours
{
    FILE *fichier = fopen(nom_fichier,"w");//on ouvre le fichier de configuration
    fprintf(fichier,"%i;%i\n%i\n",nbjoueurs,M,numero_tour);//on enregistre le nombre de joueurs et le numero du tour actuel
    int i,j;
    for(i=0;i<4;i++)
    {
        fprintf(fichier,"%s\n%i;%i\n%i;%i\n%i\n",joueurs[i].nom,joueurs[i].x,joueurs[i].y,joueurs[i].initX,joueurs[i].initY,joueurs[i].filet);//on enregistre les attributs des 4 pecheurs
    }

    for(j = 0; j < M; j++){//on parcourt toute la carte
      for(i = 0; i < M; i++){
        fprintf(fichier, "%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i\n",i,j,creature[i][j]->type_animal,//on enregistre les coordonnees ainsi que tous les attributs des creatures
                                                                           creature[i][j]->tour_courant,
                                                                           creature[i][j]->dernier_repas,
                                                                           creature[i][j]->satiete,
                                                                           creature[i][j]->derniere_reproduction,
                                                                           creature[i][j]->duree_survie,
                                                                           creature[i][j]->taille,
                                                                           creature[i][j]->taille_du_bide,
                                                                           creature[i][j]->saut_max,
                                                                           creature[i][j]->metabolisme,
                                                                           creature[i][j]->gestation,
                                                                           creature[i][j]->frequence_reproduction,
                                                                           creature[i][j]->selection,
                                                                           creature[i][j]->filet);
      }
    }
    fclose(fichier);//on ferme le fichier
}

int lire_partie_sauvegardee(const char *nom_fichier, int *numero_tour, pecheur *joueurs, animal ***creature)//permet de charger une partie sauvegardee
{
    int nbjoueurs, nb_lu,taille;
    FILE *fichier = fopen(nom_fichier,"r");//on ouvre le fichier
    fscanf(fichier,"%i;%i\n%i\n",&nbjoueurs,&taille,numero_tour);//on lit le nombre de joueurs et le numero du tour
    int i,j;
    char *nom=calloc(11,sizeof(char));
    for(i=0;i<4;i++)
    {
        nb_lu=fscanf(fichier,"%s\n%i;%i\n%i;%i\n%i\n",nom,&joueurs[i].x,&joueurs[i].y,&joueurs[i].initX,&joueurs[i].initY,&joueurs[i].filet);//on lit les attributs des pecheurs
        strcpy(joueurs[i].nom,nom);
    }


    while(nb_lu!=EOF)//tant qu'on arrive pas a la fin du fichier
    {
          fscanf(fichier, "%i;%i;",&i,&j);//on lit les coordonnees
          if(i>M || j>M)//si elles ne sont pas dans la carte
          {
              printf("\nError : Can't fit saved map into memory.\n");
              free(nom);
              return -1;//on quitte la fonction
          }
          nb_lu=fscanf(fichier, "%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i\n",(int *)&creature[i][j]->type_animal,//on lit les caracteristiques de la creature a ces coordonnees
                                                                               &creature[i][j]->tour_courant,
                                                                               &creature[i][j]->dernier_repas,
                                                                               &creature[i][j]->satiete,
                                                                               &creature[i][j]->derniere_reproduction,
                                                                               &creature[i][j]->duree_survie,
                                                                               &creature[i][j]->taille,
                                                                               &creature[i][j]->taille_du_bide,
                                                                               &creature[i][j]->saut_max,
                                                                               &creature[i][j]->metabolisme,
                                                                               &creature[i][j]->gestation,
                                                                               &creature[i][j]->frequence_reproduction,
                                                                               &creature[i][j]->selection,
                                                                               &creature[i][j]->filet);
          creature[i][j]->selection=0;
          creature[i][j]->filet=0;
    }
    fclose(fichier);//on ferme le fichier
    free(nom);
    return 0;
}
