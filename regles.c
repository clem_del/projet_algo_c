#include "regles.h"

//declaration de la table de predation
int tableau_predation[NOMBRE_ESPECES+1][NOMBRE_ESPECES+1]={{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},//rien
                                                           {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},//mer
                                                           {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},//plancton
                                                           {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0},//corail
                                                           {0,0,1,0,0,0,0,0,0,0,0,0,0,1,0},//bar
                                                           {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0},//thon
                                                           {0,0,0,1,0,0,0,0,0,0,0,0,0,0,0},//pollution
                                                           {0,0,0,0,1,1,0,0,0,0,0,1,0,0,0},//pirhanna
                                                           {0,0,0,0,1,0,0,0,0,0,0,1,0,0,0},//requin
                                                           {0,0,0,0,0,0,0,0,1,0,0,1,0,0,0},//orque
                                                           {0,0,1,0,0,0,0,0,0,0,0,0,0,1,0},//baleine
                                                           {0,0,0,0,1,1,0,1,1,1,1,0,0,0,0},//pecheur
                                                           {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},//terre
                                                           {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},//pont
                                                           {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}};//mur




int survie(animal *animal)//regle de survie
{
    if(animal->duree_survie != INFINI && animal->satiete==0 && (tour_courant - animal->dernier_repas) > animal->duree_survie)//si la creature a l'estomac vide et qu'elle n'a pas mange depuis trop longtemps
    {
        bestiaire[animal->type_animal].nombre_total--;//on decompte la creature du nombre total de creatures de cette espece
        vider(animal);//l'animal disparait
        return 0;//l'animal est mort
    }
    return 1;//l'animal a survecu
}

void tour(animal *animal)//regle de tour
{
    if((animal->satiete - animal->metabolisme) > 0)//si satiete>metabolisme
    {
        animal->satiete -= animal->metabolisme;//on retire metabolisme de satiete
    }else//si satiete<metabolisme
    {
        animal->satiete=0;//comme satiete ne peut etre negative, on prend satiete=0
    }
}

void reproduction(int x, int y, animal ***creature)//regle de reproduction
{
    int k,l,flag=1,possible=FAUX;
    int dir[3][3]={{0,0,0},{0,0,0},{0,0,0}};//tableau des possibilites
    if (creature[x][y]->frequence_reproduction!=INFINI && (creature[x][y]->satiete >= (creature[x][y]->gestation * creature[x][y]->metabolisme)) && (tour_courant) > (creature[x][y]->derniere_reproduction + creature[x][y]->frequence_reproduction))//si la creature peut se reproduire
    {
        for(k=-1;k<=1;k++)//on scanne les environs
        {
            for(l=-1;l<=1;l++)
            {
                if(est_vide(creature[x+k][y+l]))//si il y a de la place ici
                {
                    dir[k+1][l+1]=1;//on le note
                    possible=VRAI;//la reproduction est possible
                }
            }
        }
        while(flag && possible)//tant qu'on a pas trouve une case libre
        {
            k=(int)((rand()%3));//on prend des coordonnees aleatoires
            l=(int)((rand()%3));
            if(dir[k][l]==1)//si la case choisie est bien vide
            {
                creature[x][y]->satiete -= creature[x][y]->gestation*creature[x][y]->metabolisme;//on decremente la satiete
                creer_animal(creature[x+k-1][y+l-1],creature[x][y]->type_animal);//on fait apparaitre le petit
                creature[x][y]->derniere_reproduction=tour_courant;//on met a jour la derniere reproduction
                creature[x+k-1][y+l-1]->derniere_reproduction=tour_courant;//on fait en sorte que le petit ne puisse pas se reproduire tout de suite
                flag=0;//on sort de la boucle
            }
        }
    }
}

int peut_pecher(int cibleX,int cibleY,animal ***creature)//renvoie vrai si le pecheur peut pecher la cible, faux autrement
{
    if(tableau_predation[PECHEUR][creature[cibleX][cibleY]->type_animal]==1)//si il peut le pecher
    {
        return VRAI;//on retourne 1 si le pecheur peut pecher
    }
    return FAUX;//on retourne 0 si le pecheur ne peut pas pecher
}

int peut_manger(int predateurX,int predateurY,int cibleX,int cibleY,animal ***creature)//renvoie vrai si le predateur peut manger la cible, faux autrement
{
    if(tableau_predation[creature[predateurX][predateurY]->type_animal][creature[cibleX][cibleY]->type_animal]==1 && creature[predateurX][predateurY]->taille_du_bide > (creature[predateurX][predateurY]->satiete + creature[cibleX][cibleY]->taille))//si il peut le manger et que son ventre est assez vide
    {
        if(creature[cibleX][cibleY]->type_animal==PECHEUR && (creature[cibleX][cibleY]->contenu_case_actuelle==TERRE || creature[cibleX][cibleY]->contenu_case_actuelle==PONT))//si la cible est un pêcheur sur la terre ou sur un pont
        {
            return FAUX;//le pêcheur ne peut pas etre manger quand il est sur la terre ou sur un pont
        }else
        {
            return VRAI;//on retourne 1 si le predateur peut manger
        }
    }else if(tableau_predation[creature[predateurX][predateurY]->type_animal][PONT]==1 && creature[cibleX][cibleY]->type_animal==PECHEUR && creature[cibleX][cibleY]->contenu_case_actuelle==PONT && creature[predateurX][predateurY]->taille_du_bide > (creature[predateurX][predateurY]->satiete + creature[cibleX][cibleY]->taille))//si le predateur peut manger un pont et que sa cible est un pecheur sur un pont
    {
        return VRAI;//on retourne 1 si le predateur peut manger
    }
    return FAUX;//on retourne 0 si le predateur ne peut pas manger
}

int predation(int predateurX,int predateurY,int cibleX,int cibleY,animal ***creature)//regle de predation
{
    if(peut_manger(predateurX, predateurY, cibleX, cibleY, creature))//si il peut le manger et que son ventre est assez vide
    {
        creature[predateurX][predateurY]->dernier_repas=tour_courant;//il a mange a ce tour la
        creature[predateurX][predateurY]->satiete += creature[cibleX][cibleY]->taille;//on remplit le bide
        bestiaire[creature[cibleX][cibleY]->type_animal].nombre_total--;//on decompte l'animal mange
        deplacer(predateurX,predateurY,cibleX,cibleY,creature);//le pr?dateur se d?place sur le cadavre de sa victime
        return VRAI;//on retourne 1 si le predateur s'est deplace
    }
    return FAUX;//on retourne 0 si le predateur n'a pas bouge
}

void regle_predation(int i, int j, int *finali, int *finalj, animal ***creature)//predation intelligente, mange la plus grosse proie autour de lui
{
    int dir[3][3]={{-1,-1,-1},{-1,-1,-1},{-1,-1,-1}};//tableau des possibilites
    int possible=FAUX;
    int k,l; //coordonnées proie de animal courant par rapport a lui-meme

    int taille_proie_la_plus_grosse=0;
    for(k=-1;k<=1;k++)//on parcourt toute les cases attenantes pour la predation
    {
        for(l=-1;l<=1;l++)
        {
            if(peut_manger(i,j,i+k,j+l,creature))//si l'animal peut manger l'animal sur la case testee
            {
                possible=VRAI;//la creature a au moins une proie a portee
                dir[k+1][l+1]=creature[i+k][j+l]->taille;//on rajoute la case dans le tableau des possibilites
                if(creature[i+k][j+l]->taille >= taille_proie_la_plus_grosse)//on trouve la taille max des proies
                {
                    taille_proie_la_plus_grosse = creature[i+k][j+l]->taille;
                }
            }
        }
    }
    if(possible==VRAI)//si la creature a au moins une proie a portee
    {
      int flag=VRAI;
      while(flag)//tant qu'elle n'a pas mange
      {
          k=(int)((rand()%3));//on prend des coordonnees au hasard dans le tableau des possibilites
          l=(int)((rand()%3));
          if(dir[k][l]==taille_proie_la_plus_grosse)//si la case choisie est celle avec la plus grosse creature
          {
              predation(i,j,i+k-1,j+l-1,creature);//la creature mange et se deplace
              //l'animal se trouve en [i+k-1,j+l-1]
              *finali=i+k-1;//on sauve ses nouvelles coordonnees
              *finalj=j+l-1;
              flag=FAUX;//on sort de la boucle
          }
      }
    }else//si il n'y a rien a manger dans le coin
    {
        *finali=i;//la creature reste au meme endroit
        *finalj=j;
    }
}

int est_dans_carte(int x, int y)//renvoie vrai si les coordonnees appartiennent a la carte, faux autrement
{
    if(x>=0 && x<M && y>=0 && y<M)//si le point est dans la carte
    {
        return VRAI;
    }
    else {//sinon
        return FAUX;
    }
}

void deplacer(int x,int y, int newX,int newY,animal ***creature)//deplacer animal vers nouvelles coordonnees
{
    if(creature[x][y]->type_animal==PECHEUR)//si la creature a deplacer est un pecheur
    {
        espece case_actuelle=creature[x][y]->contenu_case_actuelle;//on sauvegarde la case sur laquelle etait le pecheur
        creature[x][y]->contenu_case_actuelle=creature[newX][newY]->type_animal;//on indique la case sur laquelle sera le pecheur
        *creature[newX][newY] = *creature[x][y];//on copie le pecheur a son nouvel emplacement
        creer_animal(creature[x][y],case_actuelle);//on respawn la case sur laquelle etait le pecheur
    }else//si c'est une autre creature
    {
        *creature[newX][newY] = *creature[x][y];//on copie la créature a son nouvel emplacement
        vider(creature[x][y]);//on supprime la créature a l'ancien emplacement
    }
}

//directions :
//
//          6  7  8
//          5     1
//          4  3  2
void deplacement(animal ***creature, int *x, int *y, direction direction, int nb_case)
{
    int cases_parcourues=0,ancienX,ancienY;
    const int initX=*x;
    const int initY=*y;
    if(creature[*x][*y]->satiete<nb_case)//si la creature n'a pas assez d'énergie pour faire toute la distance qu'elle veut parcourir
    {
        nb_case=creature[*x][*y]->satiete;//on la limite a ce qu'elle peut faire
    }
    do
    {
        ancienX=*x;//on sauvegarde la valeur de i et j
        ancienY=*y;
        switch(direction){//suivant la direction choisie
        case DROITE:
            *y+=1;
            cases_parcourues++;
            break;
        case DIAGONALE_BAS_DROITE:
            *x+=1;
            *y+=1;
            cases_parcourues++;
            break;
        case BAS:
            *x+=1;
            cases_parcourues++;
            break;
        case DIAGONALE_BAS_GAUCHE:
            *x+=1;
            *y-=1;
            cases_parcourues++;
            break;
        case GAUCHE:
            *y-=1;
            cases_parcourues++;
            break;
        case DIAGONALE_HAUT_GAUCHE:
            *x-=1;
            *y-=1;
            cases_parcourues++;
            break;
        case HAUT:
            *x-=1;
            cases_parcourues++;
            break;
        case DIAGONALE_HAUT_DROITE:
            *x-=1;
            *y+=1;
            cases_parcourues++;
            break;
        default://en cas d'erreur
            printf("Erreur\n");
            *x=initX;//on prend les valeurs de depart de i et j
            *y=initY;
            return;
        }
    } while(est_vide(creature[*x][*y]) && cases_parcourues<nb_case);//tant qu'on a pas parcouru le nombre de cases indique ou si il y a un obstacle

    if(!(cases_parcourues>=nb_case && est_vide(creature[*x][*y])))//si l'animal s'est arrete en rencontrant un obstacle
    {
        *x=ancienX;//on prend les avant-dernieres valeurs de i et j
        *y=ancienY;
        cases_parcourues--;//on decremente de 1 le compteur de cases parcourues
    }
    creature[initX][initY]->satiete-=cases_parcourues;//consommation de l'energie
    if(initX!=*x || initY!=*y)//si l'animal s'est déplace
    {
        deplacer(initX,initY,*x,*y,creature);//on le transfere
    }
}
