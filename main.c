#include "regles.h"
#include "menu.h"
#include "ia.h"
#include <signal.h>

pecheur joueur[4];

void gameover()//ecran de game over, appele en cas de ctrl+c
{
    vider_ecran;
    printf("\n\n\n\n\n\t\tGAME OVER\n\n\n\n\n\n\nTry Again !\n\n");
    exit(0);//on quitte le programme
}

void initialiser(animal ***creature)//procedure permettant d'allouer de l'espace-memoire pour la carte
{
    int i,j;
    //allocation mémoire
    for (i=0;i<M;i++) {
        creature[i] = (animal **)calloc(M,sizeof(animal));
        for (j=0;j<M;j++) {
            creature[i][j] = (animal *)calloc(M,sizeof(animal));
        }
    }
}

int main(int argc, char ** argv)//fonction principale, gere egalement l'aleatoire pour la distance de deplacement des creatures
{
    safe=FAUX;
    if(argc>1)
    {
        if(strcmp(argv[1],"--vgasafe")==0)//safe mode pour les consoles ne gerant pas les commandes VT100 avancees (redimensionnement)
        {
            printf("Entering safe mode (no resize)\n");
            safe=VRAI;
            pause;
        }
    }
    signal(SIGINT, gameover);//liaison ctrl+c -> game over
    M=50;
    resize(M+30)//modification taille fenetre a une valeur fixe
#ifdef __WIN32
    taille_police//selection de la taille de la police de la console (Donne le meilleurs resultats avec les "polices Raster" (Terminal) ou les caracteres sont en 8x8, pour les autres polices la taille se met a 8)
#endif
    //son;
    //system("echo \007");
    int i,j; //coordonnées animal courant
    int nbjoueurs=0;//nombre de joueurs
    int ibis,jbis; //coordonnées animal courant s'il se déplace
    int pas_gagne=VRAI;
    int nouvelle_carte=-1;
    vider_ecran;
    printf("\n\n\n\n\n\t\tLe jeu de la vie !\n\n\n\n\n\n\n");//ecran d accueil
    color("\t\t\t\tVersion 1.0 Beta\t\t\t\t\n");
    pause;
    vider_ecran;
    menu_taille_carte();
    resize(M+30)//modification taille fenetre en fonction de M
    charger_configuration_animaux();//on charge les constantes du bestiaire via un fichier de configuration ou, a defaut, avec les variables definies dans le programme
    pause;

    tour_courant=0;//on initialise le compteur de tour
    animal ***creature = (animal ***)calloc(M,sizeof(animal));//reservation de l'espace memoire de la map
    srand(time(NULL));//initialisation de l'aleatoire
    initialiser(creature);//initialisation map vide (allocation memoire)
    while(nouvelle_carte==-1)//tant que l'utilisateur n'a pas choisi
    {
        nbjoueurs=menu_mode_de_jeu(joueur);//on demande a l'utilisateur combien il y a de joueurs
        menu_nom_joueurs(nbjoueurs,joueur);//on demande les pseudos a tous les joueurs
        nouvelle_carte=menu_selection_carte(creature,nbjoueurs,joueur);//on demande a l'utilisateur quelle carte utiliser et on la charge
    }
    ajouterEnTete();//on ajoute l'en-tete du fichier de sauvegarde des statistiques
    vider_ecran;
    printf("Tour numero %d\n",tour_courant);
    afficher_carte(creature);
    ajouterEntreeTour();
    if(nouvelle_carte==VRAI)//si la carte est nouvelle
    {
        menu_sauvegarde_nouvelle_carte(creature);//on demande a l'utilisateur si il veut l'enregistrer
    }
    pause;
    while(pas_gagne)//boucle principale, tant qu'aucun joueur n'a gagne
    {
        tour_courant++;//on incremente le numero du tour
        vider_ecran;//on nettoie l'écran
        printf("Tour numero %d\n",tour_courant);
        for(i=0;i<M;i++)//on parcourt toute les cases de l'ecosysteme
        {
             for(j=0;j<M;j++)
            {
                if(creature[i][j]->tour_courant != tour_courant) {//si la creature appartient au passé
                  switch(creature[i][j]->type_animal){//selon le type de la creature
                    // Situations où ne s'appliquent pas les regles
                    case RIEN:
                    case MER:
                    case TERRE:
                    case PECHEUR:
                    case PONT:
                    case MUR:
                      break;// On n'applique pas les règles sur cette case

                    default://pour les autres
                      // On applique les règles du jeu
                      if(survie(creature[i][j]))//si la creature a survecu au tour
                      {
                          reproduction(i,j,creature);//on applique la regle de reproduction
                          regle_predation(i,j,&ibis,&jbis,creature);//on applique la regle de predation
                          if(creature[ibis][jbis]->saut_max>0)//si la creature peut se deplacer
                          {
                              deplacement(creature,&ibis,&jbis,determinerMeilleurDeplacement(creature,ibis,jbis),((rand()%(creature[ibis][jbis]->saut_max))+1));//on applique la regle de deplacement : distance : alea entre 1 et saut_max
                          }
                          tour(creature[ibis][jbis]);//on applique la regle de tour
                          //on fait passer la creature dans le futur
                          creature[ibis][jbis]->tour_courant=tour_courant;
                      }
                      break;
                   }
                }
            }
        }
        ajouterEntreeTour();//on sauvegarde le nombre de creature de chaque espece dans un fichier
        afficher_carte(creature);//on affiche la carte des creatures
        pas_gagne=menu_action_joueurs(creature,nbjoueurs,joueur);//on fait jouer les joueurs
    }
    for(i=0;i<4;i++)//si un des joueurs a gagné, on cherche lequel
    {
        if(joueur[i].Yarrivee==joueur[i].y)//si le joueur i a gagne
        {
            anim(joueur[i].nom);
        }
    }
    return 0;
}
