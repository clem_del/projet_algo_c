#ifndef REGLES_H
#define REGLES_H
#include <stdio.h>
#include <stdlib.h>
#include "animal.h"

typedef struct coordonneesPecheur{//coordonnees des pecheurs, pour ne pas a avoir a les rechercher sur toute la carte a chaque tour
    char nom[11];
    int x;
    int y;
    int initX;
    int initY;
    int Yarrivee;
    int filet;
}pecheur;

int survie(animal *animal);//regle de survie

void tour(animal *animal);//regle de tour

void reproduction(int x, int y, animal ***creature);//regle de reproduction

typedef enum{PWD,DROITE,DIAGONALE_BAS_DROITE,BAS,DIAGONALE_BAS_GAUCHE,GAUCHE,DIAGONALE_HAUT_GAUCHE,HAUT,DIAGONALE_HAUT_DROITE} direction;

int peut_pecher(int cibleX,int cibleY,animal ***creature);//retourne vrai si le pecheur peut pecher la cible

int peut_manger(int predateurX,int predateurY,int cibleX,int cibleY,animal ***creature);//retourne vrai si le predateur peut manger la cible

int predation(int predateurX, int predateurY, int cibleX, int cibleY, animal ***creature);//regle de predation

void regle_predation(int i, int j, int *finali, int *finalj, animal ***creature);//predation intelligente : prend la cible la plus grosse

int est_dans_carte(int x, int y);//retourne vrai si les coordonnees sont dans la carte

void deplacer(int x, int y, int newX, int newY, animal ***creature);//deplace une creature d'un point a un autre

void deplacement(animal ***creature, int *x, int *y, direction direction, int nb_case);//regle de deplacement


#endif // REGLES_H
