#include "menu.h"

#ifdef __APPLE__
int mygetch( ) {
  struct termios oldt,
  newt;
  int            ch;
  tcgetattr( STDIN_FILENO, &oldt );
  newt = oldt;
  newt.c_lflag &= ~( ICANON | ECHO );
  tcsetattr( STDIN_FILENO, TCSANOW, &newt );
  ch = getchar();
  tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
  return ch;
}
#elif __linux__
int mygetch( ) {
  struct termios oldt,
  newt;
  int            ch;
  tcgetattr( STDIN_FILENO, &oldt );
  newt = oldt;
  newt.c_lflag &= ~( ICANON | ECHO );
  tcsetattr( STDIN_FILENO, TCSANOW, &newt );
  ch = getchar();
  tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
  return ch;
}
#endif

void viderBuffer()//fonction de vidage du buffer de lecture
{
    int c = 0;
    while (c != '\n' && c != EOF)
    {
        c = getchar();
    }
}

int lire(char *chaine, int longueur)//fonction lisant une chaine entree sur la console et vidant le buffer apres
{
    char *positionEntree = NULL;
    if (fgets(chaine, longueur, stdin) != NULL)
    {
        positionEntree = strchr(chaine, '\n');
        if (positionEntree != NULL)
        {
            *positionEntree = '\0';
        }else
        {
            viderBuffer();
        }
        return 1;
    }else
    {
        viderBuffer();
        return 0;
    }
}

void print(char *message,int i, int j, animal ***creature)//affiche le caractere envoye en selectionnant la couleur appropriee selon selection et filet
{
    if(creature[i][j]->selection==0 && creature[i][j]->filet==0)//si la creature n'est pas selectionne et n'est pas dans le filet
    {
        printf("%s",message);//on l'affiche normalement (blanc sur fond noir)
    }else if(creature[i][j]->selection==2)//si la creature doit etre mise en avant (par exemple pecheur en train de jouer)
    {
        hcolor(message);//on l'affiche en rouge sur fond noir
    }else if(creature[i][j]->selection==1 && creature[i][j]->filet==1)//si la creature est selectionne et est dans le filet (curseur sur une case deja selectionne)
    {
        hcolor(message);//on l'affiche en rouge sur fond noir
    }else//si la creature est selectionne et n'est pas dans le filet (curseur)
    {
        color(message);//on l'affiche en couleurs inversees (noir sur fond blanc)
    }
}

void initialiser_creatures(pecheur *joueur,animal ***creature)//procedure permettant de remplir la carte de creatures aleatoirement en respectant les quotas definis pour chaque espece
{
    int i,j;
    //generation carte
    for (j=0;j<M;j++)
    {
        for(i=0;i<M;i++)
        {
            //generation map vide
            if(j==0 || j==M-1)//si on est sur le cote gauche ou droite
            {
                creer_animal(creature[i][j],TERRE);//spawn de la terre

                //placement des joueurs
                if(joueur[0].x==i && joueur[0].y==j)//si on est sur la case du joueur 1
                {
                    creer_animal(creature[i][j],PECHEUR);//spawn du joueur 1
                    creature[i][j]->contenu_case_actuelle=TERRE;
                }else if(joueur[1].x==i && joueur[1].y==j)//si on est sur la case du joueur 2
                {
                    creer_animal(creature[i][j],PECHEUR);//spawn du joueur 2
                    creature[i][j]->contenu_case_actuelle=TERRE;
                }else if(joueur[2].x==i && joueur[2].y==j)//si on est sur la case du joueur 3
                {
                    creer_animal(creature[i][j],PECHEUR);//spawn du joueur 3
                    creature[i][j]->contenu_case_actuelle=TERRE;
                }else if(joueur[3].x==i && joueur[3].y==j)//si on est sur la case du joueur 4
                {
                    creer_animal(creature[i][j],PECHEUR);//spawn du joueur 4
                    creature[i][j]->contenu_case_actuelle=TERRE;
                }else if(i==(M-1)/2 && (j==0 || j==M-1))//si on est au milieu du cote
                {
                    creer_animal(creature[i][j],MUR);//spawn du mur de separation
                }
            } else if (i==0 || i==M-1)//si on est sur le cote haut ou bas
            {
                creer_animal(creature[i][j],MUR);//spawn des murs
            }
            else//si on est ailleurs dans la carte
            {
                vider(creature[i][j]);//spawn de la mer
            }
        }
    }


    //placement des creatures
    int flag=VRAI,nb_creatures_cree=0,nb_creature_en_cours=0,espece_en_cours=MER;
    //float pourcentage=0;
    for(espece_en_cours=MER;espece_en_cours<=TERRE;espece_en_cours++)//pour toutes les especes
    {
        flag=VRAI;
        nb_creature_en_cours=0;
        while(flag)//tant qu'on a pas place toutes les creatures de l'espece en cours
        {
            if(bestiaire[espece_en_cours].taux_apparition==nb_creature_en_cours)//si l'on a place toutes les creatures de l'espece en cours
            {
                flag=FAUX;//on sort de la boucle
            }else//sinon
            {
                i=rand()%(M-2)+1;//on prend des coordonnees au hasard
                j=rand()%(M-2)+1;
                //pourcentage=(float)((float)nb_creatures_cree/((M-2)*(M-2)))*100;//calcul du pourcentage d'avancement

                //vider_ecran;
                //printf("Loading... %lf %%\n",pourcentage);//affichage de l'avancement, ralentit la creation de la carte
                //afficher_carte(creature);
                if(est_vide(creature[i][j]))//si la case choisie est vide (mer)
                {
                    creer_animal(creature[i][j],espece_en_cours);//on place une creature de l'espece en cours
                    nb_creatures_cree++;
                    nb_creature_en_cours++;
                }
                //printf("En cours : %d sur %d | Espece en cours : %i\n",nb_creature_en_cours,bestiaire[espece_en_cours].taux_apparition,espece_en_cours);//affichage de l'avancement, ralentit la creation de la carte
            }
        }
    }
}

void afficher_legende(int i)//afficher la legende a cote de la carte
{
    switch(i)//selon la ligne a laquelle on se trouve
    {
    case 0:
        printf("###############\t");
        break;
    case 1:
        printf("# ");rouge("# terre");printf("     #\t");
        break;
    case 2:
        printf("# ");rouge(". plancton");printf("  #\t");
        break;
    case 3:
        printf("# ");rouge("c corail");printf("    #\t");
        break;
    case 4:
        printf("# ");vert("b bar");printf("       #\t");
        break;
    case 5:
        printf("# ");vert("t thon");printf("      #\t");
        break;
    case 6:
        printf("# ");rouge("- pollution");printf(" #\t");
        break;
    case 7:
        printf("# ");vert("p pirhanna");printf("  #\t");
        break;
    case 8:
        printf("# ");vert("r requin");printf("    #\t");
        break;
    case 9:
        printf("# ");vert("o orque");printf("     #\t");
        break;
    case 10:
        printf("# ");vert("B baleine");printf("   #\t");
        break;
    case 11:
        printf("# ");rouge("= pont");printf("      #\t");
        break;
    case 12:
        printf("# ");rouge("X mur");printf("       #\t");
        break;
    case 13:
        printf("# ");rouge("@ pecheur");printf("   #\t");
        break;
    case 14:
        printf("###############\t");
        break;
    default:
        printf("\t\t");
        break;
    }
}

void afficher_carte(animal ***creature)//procedure permettant d'afficher la carte sur la console, utilise print()
{
    printf("\n\n");
    int i,j;
    for (i=0;i<M;i++)//on parcourt toute la carte
    {
        afficher_legende(i);
        for(j=0;j<M;j++)
        {
            switch(creature[i][j]->type_animal){//suivant le type d'animal de la case actuel
            case MER:
                print(" ",i,j,creature);//on affiche le caractere associe
                break;
            case TERRE:
                print("#",i,j,creature);//on affiche le caractere associe
                break;
            case PLANCTON:
                print(".",i,j,creature);//on affiche le caractere associe
                break;
            case CORAIL:
                print("c",i,j,creature);//on affiche le caractere associe
                break;
            case BAR:
                print("b",i,j,creature);//on affiche le caractere associe
                break;
            case THON:
                print("t",i,j,creature);//on affiche le caractere associe
                break;
            case POLLUTION:
                print("-",i,j,creature);//on affiche le caractere associe
                break;
            case PIRHANNA:
                print("p",i,j,creature);//on affiche le caractere associe
                break;
            case REQUIN:
                print("r",i,j,creature);//on affiche le caractere associe
                break;
            case ORQUE:
                print("o",i,j,creature);//on affiche le caractere associe
                break;
            case BALEINE:
                print("B",i,j,creature);//on affiche le caractere associe
                break;
            case PECHEUR:
                print("@",i,j,creature);//on affiche le caractere associe
                break;
            case PONT:
                print("=",i,j,creature);//on affiche le caractere associe
                break;
            case MUR:
                print("X",i,j,creature);//on affiche le caractere associe
                break;
            default:
                print("X",i,j,creature);//on affiche le caractere par defaut
                break;
            }
        }
        printf("\n");
    }
    printf("\n");
}

int ISA(int mode, int numero_joueur, pecheur *joueur, animal ***creature)//fonction permettant de selectionner une ou plusieurs cases graphiquement sur la console avec les touches zqsd et d'effectuer l'action voulue par le joueur (peche, deplacement, ...)
{
    int dir=0;
    direction direction;
    int distance_max=1;
    int compteur_creatures_selectionnees=0;
    vider_ecran;
    color("Bienvenue dans l'Interface de Selection Automatisee !\n");//message de bienvenue
    printf("\nVous avez choisi de ");//affichage de la fonction selectionnee
    switch(mode){//suivant le mode choisi
    case 0:
        printf("vous deplacer\n");//on affiche l'operation qui a ete selectionnee
        distance_max=1;//on initialise la distance a laquelle on peut s'eloigner du joueur pour ce mode, ici deplacement de 1 case maxi
        break;
    case 1:
        printf("pecher\n");
        distance_max=creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->taille_canne_a_peche;//peche jusqu'a taille_canne_a_peche
        break;
    case 2:
        printf("construire un pont\n");
        distance_max=1;//construction de ponts jusqu'a 1 bloc de distance
        break;
    case 3:
        printf("balancer un poisson\n");
        distance_max=3*creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->taille_canne_a_peche;//lancement de poisson jusqu'a 3*taille_canne_a_peche
        break;
    case 4:
        printf("pecher a l'epuisette\n");
        distance_max=1;//pecher a l'epuisette jusqu'a 1 bloc de distance
        break;
    case 5:
        printf("pecher au filet\n");
        distance_max=creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->distance_peche_filet;//peche au filet jusqu'a distance_peche_filet
        break;
    default:
        printf("ne rien faire...paresseux !\n");//si on veut passer le tour
        pause;
        return 1;//on quitte la fonction
    }
    //mise en avant joueur en cours
    creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->selection=2;
    //initialisation du curseur
    int *curseurX=malloc(sizeof(int));
    *curseurX=joueur[numero_joueur].x;
    int *curseurY=malloc(sizeof(int));
    *curseurY=joueur[numero_joueur].y;

    afficher_carte(creature);//on reaffiche la carte

    int tab_peche_epuisette[8][2]={{-1,-1},//tableau des prises pour la peche a l'epuisette
                               {-1,-1},
                               {-1,-1},
                               {-1,-1},
                               {-1,-1},
                               {-1,-1},
                               {-1,-1},
                               {-1,-1}};

    int flag=VRAI;
    int k,i=0,tempX,tempY;

    do//tant que l'utilisateur n'a pas fini
    {
        printf("Selectionner la case avec z q s d, echap pour annuler\nAppuyez sur entree pour valider\n");//on affiche la doc
        if(mode==4)
        {
            printf("Pour selectionner plusieurs cases, appuyez sur p\n");
        }
        dir=lire_un();//on lit la touche appuyee par l'utilisateur
        switch(dir){//suivant la touche appuyee
        case 'z'://si c'est z
            direction=HAUT;//on va en haut
            break;
        case 'q'://si c'est q
            direction=GAUCHE;//on va a gauche
            break;
        case 's'://si c'est s
            direction=BAS;//on va en bas
            break;
        case 'd'://si c'est d
            direction=DROITE;//on va a droite
            break;
        case 'p'://si c'est p
            if(mode==4)//si on utilise l'epuisette
            {
                compteur_creatures_selectionnees++;//on augmente le compteur de bestioles dans l'epuisette
                if(compteur_creatures_selectionnees<9)//si le compteur de bestioles est inferieur a 9
                {
                    if(peut_pecher(*curseurX,*curseurY,creature))//si on peut pecher la creature prise dans l'epuisette
                    {
                        if(creature[*curseurX][*curseurY]->filet==1)//si elle etait deja dans l'epuisette
                        {
                            creature[*curseurX][*curseurY]->filet=0;//on la deselectionne
                            compteur_creatures_selectionnees-=2;//on l'ote du compteur de bestioles
                            i=0;
                            while(tab_peche_epuisette[i][0]!=*curseurX || tab_peche_epuisette[i][1]!=*curseurY)//tant que l'on n'a pas retrouve la bestiole dans le tableau de bestioles
                            {
                                i++;//on passe a l'index suivant
                            }
                            tab_peche_epuisette[i][0]=-1;//on efface l'entree de la bestiole dans le tableau de bestioles
                            tab_peche_epuisette[i][1]=-1;
                            for(k=i;k<compteur_creatures_selectionnees;k++)//tant qu'on a pas atteint la fin du tableau
                            {
                                tempX=tab_peche_epuisette[k][0];//on decale tous les elements du tableau pour que les -1 se retrouvent a la fin
                                tempY=tab_peche_epuisette[k][1];
                                tab_peche_epuisette[k][0]=tab_peche_epuisette[k+1][0];
                                tab_peche_epuisette[k][1]=tab_peche_epuisette[k+1][1];
                                tab_peche_epuisette[k+1][0]=tempX;
                                tab_peche_epuisette[k+1][1]=tempY;
                            }
                        }else//si elle n'etait pas dans l'epuisette
                        {
                            creature[*curseurX][*curseurY]->filet=1;//on la rajoute dans le tableau de bestioles
                            tab_peche_epuisette[compteur_creatures_selectionnees-1][0]=*curseurX;
                            tab_peche_epuisette[compteur_creatures_selectionnees-1][1]=*curseurY;
                        }
                    }else//si on ne peut pas pecher la creature prise dans l'epuisette
                    {
                        printf("Vous ne pouvez pas pecher ce truc !\n");
                        compteur_creatures_selectionnees--;//on retire la creature de l'epuisette
                        pause;
                    }
                }else//si le compteur de bestioles est au dessus de 8
                {
                    printf("Votre epuisette va craquer si vous prenez un poisson de plus !\n");
                    compteur_creatures_selectionnees=8;//on reste a 8
                    pause;
                }
            }
            direction=PWD;//on ne bouge pas
            break;
        case touche_entree://si on appuie sur entree
            if(mode==0)//mode deplacement
            {
                if(*curseurX==joueur[numero_joueur].x && *curseurY==joueur[numero_joueur].y)//si le curseur est sur le joueur
                {
                    flag=VRAI;//on reste dans la boucle
                    direction=PWD;//on ne bouge pas le curseur
                }else//sinon
                {
                    if(creature[*curseurX][*curseurY]->type_animal==TERRE || creature[*curseurX][*curseurY]->type_animal==PONT)//si la case indiquee est valide
                    {
                        deplacer(joueur[numero_joueur].x,joueur[numero_joueur].y,*curseurX,*curseurY,creature);//on deplace le pecheur
                        joueur[numero_joueur].x=*curseurX;//on met a jour ses coordonnees
                        joueur[numero_joueur].y=*curseurY;
                        flag=FAUX;//on sort de la boucle
                    }else//sinon
                    {
                        flag=VRAI;//on reste dans la boucle
                    }
                }
            }else if(mode==1)//mode peche
            {
                if(peut_pecher(*curseurX,*curseurY,creature))//si on peut pecher la creature indiquee
                {
                    bestiaire[creature[*curseurX][*curseurY]->type_animal].nombre_total--;//on decremente le compteur d'animal de cette race
                    creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->derniere_prise=creature[*curseurX][*curseurY]->type_animal;//on sauve la derniere prise
                    creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->sac+=creature[*curseurX][*curseurY]->taille;//on met la creature dans le sac
                    vider(creature[*curseurX][*curseurY]);//on vide la case ou l'on a peche
                    flag=FAUX;//on sort de la boucle
                }else//sinon
                {
                    printf("Vous ne pouvez pas pecher ce truc !\n");
                    pause;
                    flag=VRAI;//on reste dans la boucle
                }
            }else if(mode==2)//mode construction
            {
                if(creature[*curseurX][*curseurY]->type_animal==MER)//si la case indiquee contient de la mer
                {
                    creer_animal(creature[*curseurX][*curseurY],PONT);//on cree un pont a cet endroit
                    creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->derniere_prise=MER;//on reset la derniere prise
                    creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->sac--;//on vide le sac
                    flag=FAUX;//on sort de la boucle
                }
            }else if(mode==3)//mode lancer de poisson
            {
                if(creature[*curseurX][*curseurY]->type_animal==MER)//si la case indiquee contient de la mer
                {
                    creer_animal(creature[*curseurX][*curseurY],creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->derniere_prise);//on recree un animal du meme type que le dernier peche
                    creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->sac-=bestiaire[creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->derniere_prise].taille;//on vide le sac en consequence
                    creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->derniere_prise=MER;//on reset la derniere prise
                    flag=FAUX;//on sort de la boucle
                }
            }else if(mode==4)//mode peche a l'epuisette
            {
                if(compteur_creatures_selectionnees>0)//si l'on a pris quelque chose
                {
                    i=0;
                    while(tab_peche_epuisette[i][0]!=-1)//tant qu'on arrive pas au bout du tableau
                    {
                        bestiaire[creature[tab_peche_epuisette[i][0]][tab_peche_epuisette[i][1]]->type_animal].nombre_total--;//on decremente le nombre totale de la creature prise dans l'epuisette
                        creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->derniere_prise=creature[tab_peche_epuisette[i][0]][tab_peche_epuisette[i][1]]->type_animal;//on met a jour la derniere prise
                        creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->sac+=creature[tab_peche_epuisette[i][0]][tab_peche_epuisette[i][1]]->taille;//on augmente le sac en consequence
                        vider(creature[tab_peche_epuisette[i][0]][tab_peche_epuisette[i][1]]);//on vide la case ou l'on a peche
                        flag=FAUX;//on sort de la boucle
                        creature[tab_peche_epuisette[i][0]][tab_peche_epuisette[i][1]]->filet=0;//on deselectionne la case
                        i++;//on passe a la prise suivante dans l'epuisette
                    }
                    creature[*curseurX][*curseurY]->selection=0;//on deselectionne l'emplacement du curseur
                }else//si l'on a rien pris
                {
                    printf("vous n'avez rien pris !\n");
                    pause;
                }
            }else if(mode==5)//mode peche au filet
            {
                for(i=-1;i<2;i++)//on parcourt toutes les cases attenantes
                {
                    for(k=-1;k<2;k++)
                    {
                        if(est_dans_carte(i+(*curseurX),k+(*curseurY)))//si on est dans la carte
                        {
                            creature[i+(*curseurX)][k+(*curseurY)]->filet=0;//on deselectionne la case
                            if(peut_pecher(i+(*curseurX),k+(*curseurY),creature))//si on peut pecher la creature
                            {
                                bestiaire[creature[i+(*curseurX)][k+(*curseurY)]->type_animal].nombre_total--;//on decremente le nombre totale de la creature
                                creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->derniere_prise=creature[i+(*curseurX)][k+(*curseurY)]->type_animal;//on met a jour la derniere prise
                                creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->sac+=creature[i+(*curseurX)][k+(*curseurY)]->taille;//on augmente le sac en consequence
                                vider(creature[i+(*curseurX)][k+(*curseurY)]);//on vide la case ou l'on a peche
                                flag=FAUX;//on sort de la boucle
                            }
                        }
                    }
                }
                creature[*curseurX][*curseurY]->selection=0;//on deselectionne l'emplacement du curseur
            }
            direction=PWD;//on ne bouge pas
            break;
        case 27://touche echap, on annule tout
            creature[*curseurX][*curseurY]->selection=0;//on deselectionne l'emplacement du curseur
            if(mode==4)//si on peche a l'epuisette
            {
                i=0;
                while(tab_peche_epuisette[i][0]!=-1)//tant qu'on arrive pas a la fin du tableau de bestioles
                {
                    creature[tab_peche_epuisette[i][0]][tab_peche_epuisette[i][1]]->filet=0;//on deselectionne la bestiole en cours
                }
            }else if(mode==5)//si on peche au filet
            {
                for(i=-1;i<2;i++)//on parcourt les cases proches
                {
                    for(k=-1;k<2;k++)
                    {
                        if(est_dans_carte(i+(*curseurX),k+(*curseurY)))//si on est dans la carte
                        {
                            creature[i+(*curseurX)][k+(*curseurY)]->filet=0;//on deselectionne la case
                        }
                    }
                }
            }
            printf("Annulation...\n");
            free(curseurX);
            free(curseurY);
            return 0;//on sort de la fonction
        default://appui sur une autre touche
            direction=PWD;//on ne bouge pas
            break;
        }
        if(mode==5)//si on peche au filet
        {
            for(i=-1;i<2;i++)//on parcourt les cases proches
            {
                for(k=-1;k<2;k++)
                {
                    if(est_dans_carte(i+(*curseurX),k+(*curseurY)))//si on est dans la carte
                    {
                        creature[i+(*curseurX)][k+(*curseurY)]->filet=0;//on deselectionne la case
                    }
                }
            }
        }
        if(flag==VRAI)//si on ne sort pas de la boucle
        {
            deplacer_curseur(joueur[numero_joueur].x,joueur[numero_joueur].y,curseurX,curseurY,direction,distance_max,creature);//on deplace le curseur dans la direction voulue
        }
        if(mode==5)//si on peche au filet
        {
            for(i=-1;i<2;i++)//on parcourt les cases proches
            {
                for(k=-1;k<2;k++)
                {
                    if(est_dans_carte(i+(*curseurX),k+(*curseurY)))//si on est dans la carte
                    {
                        creature[i+(*curseurX)][k+(*curseurY)]->filet=1;//on selectionne la case
                    }
                }
            }
        }
        vider_ecran;
        afficher_carte(creature);//on affiche la carte
    }while(flag);//tant que le joueur n'a pas fini
    if(mode==5)//si on peche au filet
    {
        for(i=-1;i<2;i++)//on parcourt les cases proches
        {
            for(k=-1;k<2;k++)
            {
                if(est_dans_carte(i+(*curseurX),k+(*curseurY)))//si on est dans la carte
                {
                    creature[i+(*curseurX)][k+(*curseurY)]->filet=0;//on selectionne la case
                }
            }
        }
    }
    creature[joueur[numero_joueur].x][joueur[numero_joueur].y]->selection=0;//deselection pecheur
    free(curseurX);
    free(curseurY);
    return 1;
}

char * selecteur_slot(int mode)//fonction permettant de selectionner un slot de sauvegarde graphiquement, renvoie le nom du fichier a utiliser
{
    char *nomFichier=calloc(99,sizeof(char));
    char *extension=calloc(5,sizeof(char));
    FILE *fichier;
    switch(mode){//selection de l'extension du fichier a charger
    case 0://selection carte a lire
        strcpy(extension,".map");
        break;
    case 1://selection partie sauvegardee
        strcpy(extension,".sav");
        break;
    default:
        printf("Erreur, mode inconnu\n");
        return NULL;//on sort de la fonction
    }
    int i;
    int dispo[5]={0};
    for(i=0;i<5;i++)//verification d'existence des differents fichiers
    {
        sprintf(nomFichier,"%d%s",i,extension);
        fichier = fopen(nomFichier,"r");//on ouvre le fichier
        if(fichier!=NULL)//si le fichier s'est ouvert (et donc qu'il existe)
        {
            dispo[i]=1;//on note le fichier comme present
            fclose(fichier);//on le ferme
        }else//si le fichier ne s'est pas ouvert
        {
            dispo[i]=0;//le fichier est absent
        }
    }


    int flag=VRAI;
    int dir=0;
    int sel=0;
    char *message=calloc(50,sizeof(char));
    int nbjoueurs,taille;
    do//tant que le joueur n'a pas choisi un slot
    {
        vider_ecran;
        printf("Selectionner le slot avec z et s, echap pour annuler\n");
        for(i=0;i<5;i++)// on parcourt les slots
        {
            if(dispo[i]==1)//si il est dispo
            {
                if(mode==1)//si on est en mode sauvegarde/restauration de partie
                {
                    sprintf(nomFichier,"%d%s",i,extension);//on construit le nom du fichier
                    fichier = fopen(nomFichier,"r");//on l'ouvre
                    fscanf(fichier,"%i;%i",&nbjoueurs,&taille);//on lit le nombre de joueurs et la taille de la carte pour cette sauvegarde
                    fclose(fichier);//on ferme le fichier
                    if(nbjoueurs>1)//si le nombre de joueurs est superieur a 1
                    {
                        sprintf(message,"%s%d%s%i%s%i%s","\n\n\t\t\tSlot ",i," (",nbjoueurs," joueurs, taille ",taille,")\t\t\t");//on affiche le slot
                    }else//sinon
                    {
                        sprintf(message,"%s%d%s%i%s%i%s","\n\n\t\t\tSlot ",i," (",nbjoueurs," joueur, taille ",taille,")\t\t\t");//on affiche le slot (sans "s" a joueur)
                    }
                }else//si on est en mode selection/enregistrement de carte
                {
                    sprintf(nomFichier,"%d%s",i,extension);//on construit le nom du fichier
                    fichier = fopen(nomFichier,"r");//on l'ouvre
                    fscanf(fichier,"%i",&taille);//on lit la taille de la carte pour cette sauvegarde
                    fclose(fichier);//on ferme le fichier
                    sprintf(message,"%s%d%s%i%s","\n\n\t\t\tSlot ",i,"(taille ",taille,")\t\t\t");//on affiche le slot
                }
            }else if(dispo[i]==0)//si il n'est pas present
            {
                sprintf(message,"%s%d%s","\n\n\t\t\tSlot ",i," (vide)\t\t\t");//on affiche le slot (vide)
            }
            if(sel==i)//si le slot actuel est selectionne
            {
                color(message);//on l'affiche en couleur inversee
            }else//sinon
            {
                printf("%s",message);//on l'affiche normalement
            }
        }
        dir=lire_un();//on lit la touche entree par l'utilisateur
        switch(dir){//selon la touche appuyee
        case 'z'://si c'est z
            if(sel>0)//si on est pas tout en haut
            {
                sel--;//on remonte d'un slot le curseur
            }
            break;
        case 's'://si c'est s
            if(sel<4)//si on est pas tout en bas
            {
                sel++;//on descend l'un slot le curseur
            }
            break;
        case touche_entree://si c'est la touche entree
            flag=FAUX;//on sort de la boucle
            sprintf(nomFichier,"%d%s",sel,extension);//on construit le nom du fichier selon l'emplacement du curseur
            return nomFichier;//on retourne le nom du fichier choisi
            break;
        case 27://si c'est la touche echap (annulation)
            return NULL;//on retourne NULL
            break;
        default://si c'est une autre touche
            break;//on ne fait rien
        }
    }while(flag);//tant que l'on a pas choisi
    return NULL;
}

int menu_mode_de_jeu(pecheur *joueur)//fonction permettant de faire choisir a l'utilisateur le nombre de joueurs, renvoie le nombre de joueurs
{
    char *buffer=calloc(100,sizeof(char));//buffer de lecture
    int joueurs=0;
    //menu de selection du mode de jeu
    vider_ecran;
    printf("\n\n\t\t\tCombien de joueurs ?");
    printf("\n\n\n\n\t\t\t(0) Aucun joueur...\n\n\n\t\t\t(1) Solo\n\n\n\t\t\t(2-4) Multijoueur local\n\n\n");
    do
    {
        printf("\t\t\tQuel est votre choix ? ");
        lire(buffer,99);
    }while(strcmp(buffer,"0")!=0 && strcmp(buffer,"1")!=0 && strcmp(buffer,"2")!=0 && strcmp(buffer,"3")!=0 && strcmp(buffer,"4")!=0);//tant que l'entree n'est pas correcte
    //initialisation des noms par defaut pour les joueurs
    strcpy(joueur[0].nom,"Joueur_1");
    strcpy(joueur[1].nom,"Joueur_2");
    strcpy(joueur[2].nom,"Joueur_3");
    strcpy(joueur[3].nom,"Joueur_4");

    if(strcmp(buffer,"1")==0)//initialisation des coordonnees du ou des joueurs
    {
        joueurs=1;
        joueur[0].y=M-1;
        joueur[0].x=(M-1)/2+1;
        joueur[1].y=-1;
        joueur[1].x=-1;
        joueur[2].y=-1;
        joueur[2].x=-1;
        joueur[3].y=-1;
        joueur[3].x=-1;

    }else if(strcmp(buffer,"2")==0)
    {
        joueurs=2;
        joueur[0].y=M-1;
        joueur[0].x=(M-1)/2+1;
        joueur[1].y=0;
        joueur[1].x=(M-1)/2+1;
        joueur[2].y=-1;
        joueur[2].x=-1;
        joueur[3].y=-1;
        joueur[3].x=-1;
    }else if(strcmp(buffer,"3")==0)
    {
        joueurs=3;
        joueur[0].y=M-1;
        joueur[0].x=(M-1)/2+1;
        joueur[1].y=0;
        joueur[1].x=(M-1)/2+1;
        joueur[2].y=M-1;
        joueur[2].x=(M-1)/2-1;
        joueur[3].y=-1;
        joueur[3].x=-1;
    }else if(strcmp(buffer,"4")==0)
    {
        joueurs=4;
        joueur[0].y=M-1;
        joueur[0].x=(M-1)/2+1;
        joueur[1].y=0;
        joueur[1].x=(M-1)/2+1;
        joueur[2].y=M-1;
        joueur[2].x=(M-1)/2-1;
        joueur[3].y=0;
        joueur[3].x=(M-1)/2-1;
    }else
    {
        joueurs=0;
        joueur[0].y=-1;
        joueur[0].x=-1;
        joueur[1].y=-1;
        joueur[1].x=-1;
        joueur[2].y=-1;
        joueur[2].x=-1;
        joueur[3].y=-1;
        joueur[3].x=-1;
    }
    //initialisation des coordonnees de base des joueurs
    joueur[0].initY=M-1;
    joueur[0].initX=(M-1)/2+1;
    joueur[1].initY=0;
    joueur[1].initX=(M-1)/2+1;
    joueur[2].initY=M-1;
    joueur[2].initX=(M-1)/2-1;
    joueur[3].initY=0;
    joueur[3].initX=(M-1)/2-1;

    //initialisation du filet
    joueur[0].filet=FAUX;
    joueur[1].filet=FAUX;
    joueur[2].filet=FAUX;
    joueur[3].filet=FAUX;

    //initialisation des points d'arrivee des joueurs
    joueur[0].Yarrivee=0;
    joueur[1].Yarrivee=M-1;
    joueur[2].Yarrivee=0;
    joueur[3].Yarrivee=M-1;

    return joueurs;//on retourne le nombre de joueurs participant a la partie
}

int menu_selection_carte(animal ***creature, int joueurs, pecheur *joueur)//fonction permettant de demander a l'utilisateur quelle carte charger, puis charge la carte, renvoie 1 si c'est une nouvelle carte, 0 autrement
{
    //menu de selection de cartes
    int carte_selectionnee=FAUX;
    int nouvelle_carte=FAUX;
    int chargement_reussi=FAUX;
    char *buffer=calloc(100,sizeof(char));//buffer de lecture
    char *fichier;
    FILE *fichier_selection;
    while(carte_selectionnee==FAUX)//tant que l'on a pas selectionne de carte
    {
        vider_ecran;
        printf("\n\n\t\t\tQuelle carte ?");
        printf("\n\n\n\n\t\t\t(0) Nouvelle carte\n\n\n\t\t\t(1) Charger une carte sauvegardee\n\n\n\t\t\t(2) Charger une partie sauvegardee\n\n\n\t\t\t(3) Revenir au menu precedent\n\n\n");
        do
        {
            printf("\t\t\tQuel est votre choix ? ");
            lire(buffer,99);
        }while(strcmp(buffer,"0")!=0 && strcmp(buffer,"1")!=0 && strcmp(buffer,"2")!=0 && strcmp(buffer,"3")!=0);//tant que l'entree n'est pas correcte
        if(strcmp(buffer,"1")==0)//chargement d'une carte
        {
            //affichage menu de selection des cartes
            fichier=selecteur_slot(0);
            if(fichier!=NULL)//si on a pas annule
            {
                fichier_selection=fopen(fichier,"r");//on ouvre le fichier
                if(fichier_selection!=NULL)//si le fichier existe
                {
                    int taille;
                    fscanf(fichier_selection,"%i",&taille);
                    fclose(fichier_selection);//on ferme le fichier
                    if(taille==M)//si la taille correspond
                    {
                        int chargement_reussi=chargement_carte(fichier,creature);//on charge la carte
                        if(joueurs==0)//adaptation de la carte chargee au nombre de joueurs selectionnes
                        {
                            creer_animal(creature[joueur[0].initX][joueur[0].initY],TERRE);
                            creer_animal(creature[joueur[1].initX][joueur[1].initY],TERRE);
                            creer_animal(creature[joueur[2].initX][joueur[2].initY],TERRE);
                            creer_animal(creature[joueur[3].initX][joueur[3].initY],TERRE);
                        }else if(joueurs==1)
                        {
                            creer_animal(creature[joueur[0].initX][joueur[0].initY],PECHEUR);
                            creer_animal(creature[joueur[1].initX][joueur[1].initY],TERRE);
                            creer_animal(creature[joueur[2].initX][joueur[2].initY],TERRE);
                            creer_animal(creature[joueur[3].initX][joueur[3].initY],TERRE);
                        }else if(joueurs==2)
                        {
                            creer_animal(creature[joueur[0].initX][joueur[0].initY],PECHEUR);
                            creer_animal(creature[joueur[1].initX][joueur[1].initY],PECHEUR);
                            creer_animal(creature[joueur[2].initX][joueur[2].initY],TERRE);
                            creer_animal(creature[joueur[3].initX][joueur[3].initY],TERRE);
                        }else if(joueurs==3)
                        {
                            creer_animal(creature[joueur[0].initX][joueur[0].initY],PECHEUR);
                            creer_animal(creature[joueur[1].initX][joueur[1].initY],PECHEUR);
                            creer_animal(creature[joueur[2].initX][joueur[2].initY],PECHEUR);
                            creer_animal(creature[joueur[3].initX][joueur[3].initY],TERRE);
                        }else
                        {
                            creer_animal(creature[joueur[0].initX][joueur[0].initY],PECHEUR);
                            creer_animal(creature[joueur[1].initX][joueur[1].initY],PECHEUR);
                            creer_animal(creature[joueur[2].initX][joueur[2].initY],PECHEUR);
                            creer_animal(creature[joueur[3].initX][joueur[3].initY],PECHEUR);
                        }
                        if(chargement_reussi==VRAI)//si on a reussi a charger la map jusqu'au bout
                        {
                            carte_selectionnee=VRAI;//on sort de la boucle
                        }else//sinon
                        {
                            pause;
                            carte_selectionnee=FAUX;//on reste dans la boucle
                        }
                    }else//si la taille ne correspond pas
                    {
                        printf("\nErreur : reglages differents !\n");
                        carte_selectionnee=FAUX;//on reste dans la boucle
                        pause;
                    }

                }else//si le fichier n'existe pas
                {
                    printf("\nCe slot est vide !\n");
                    pause;
                    carte_selectionnee=FAUX;//on reste dans la boucle
                }
            }else//si on a annule
            {
                carte_selectionnee=FAUX;//on reste dans la boucle
            }
        }else if(strcmp(buffer,"2")==0)//chargement partie sauvegardee
        {
            fichier=selecteur_slot(1);//on selectionne un slot
            if(fichier!=NULL)//si on a pas annule
            {
                fichier_selection=fopen(fichier,"r");//on ouvre le fichier
                if(fichier_selection!=NULL)//si le fichier existe
                {
                    int nb,taille;
                    fscanf(fichier_selection,"%i;%i",&nb,&taille);//on lit le nombre de joueurs dans le fichier
                    fclose(fichier_selection);//on ferme le fichier
                    if(nb==joueurs && taille==M)//si le nombre de joueurs correspond au nombre de joueurs choisi et que la taille correspond
                    {
                        chargement_reussi=lire_partie_sauvegardee(fichier, &tour_courant, joueur, creature);//on charge la carte
                        if(chargement_reussi==-1)//si le chargement a echoue
                        {
                            carte_selectionnee=FAUX;//on reste dans la boucle
                            pause;
                        }else//sinon
                        {
                            carte_selectionnee=VRAI;//on sort de la boucle
                        }
                    }else//si le nombre de joueurs ou la taille ne correspondent pas
                    {
                        printf("\nErreur : reglages differents !\n");
                        carte_selectionnee=FAUX;//on reste dans la boucle
                        pause;
                    }

                }else//si le fichier n'existe pas
                {
                    printf("\nCe slot est vide !\n");
                    pause;
                    carte_selectionnee=FAUX;//on reste dans la boucle
                }
            }else//si on a annule
            {
                carte_selectionnee=FAUX;//on reste dans la boucle
            }
        }else if(strcmp(buffer,"3")==0)//retour au menu precedent
        {
            nouvelle_carte=-1;
            carte_selectionnee=VRAI;//on sort de la boucle
        }
        else//creation nouvelle carte
        {
            initialiser_creatures(joueur,creature);//on remplit la carte
            carte_selectionnee=VRAI;//on sort de la boucle
            nouvelle_carte=VRAI;//il s'agit d'une nouvelle carte
        }
    }
    return nouvelle_carte;
}

void menu_sauvegarde_nouvelle_carte(animal ***creature)//procedure permettant de demander a l'utilisateur si il veut sauvegarder la carte, si oui elle appelle le selecteur de slots et enregistre la carte
{
    char *fichier;
    char *buffer=calloc(100,sizeof(char));//buffer de lecture
    printf("Voulez-vous sauvegarder cette carte ?\n\n\t\t\t(0) Oui \n\n\t\t\t(1) Non\n\n");
    do
    {
        printf("Quel est votre choix ? ");
        lire(buffer,99);
    }while(strcmp(buffer,"0")!=0 && strcmp(buffer,"1")!=0);//tant que l'entree n'est pas correcte
    if(strcmp(buffer,"0")==0)//sauvegarde de la carte
    {
        fichier=selecteur_slot(0);
        if(fichier!=NULL)
        {
            enregistrer_carte(fichier,creature);
        }
    }
    free(buffer);
}

int menu_action_joueurs(animal ***creature, int joueurs, pecheur *joueur)//fonction permettant de demander aux joueurs ce qu'il veulent faire et effectue les actions appropriés a l'aide d'autres fonctions, renvoie FAUX si un des joueurs a gagne, VRAI autrement
{
    //gestion du ou des pêcheurs
    int pas_gagne=VRAI;
    char *fichier;
    char *buffer=calloc(100,sizeof(char));//buffer de lecture
    int i,j;

    for(i=0;i<joueurs;i++)//tour de tous les joueurs
    {
        if(creature[joueur[i].x][joueur[i].y]->type_animal!=PECHEUR)//si la creature a l'emplacement du pecheur n'est pas un pecheur
        {
            joueur[i].x=joueur[i].initX;//on remet le joueur a son point de spawn
            joueur[i].y=joueur[i].initY;
            creer_animal(creature[joueur[i].x][joueur[i].y],PECHEUR);//on le respawn
            creature[joueur[i].x][joueur[i].y]->contenu_case_actuelle=TERRE;
            printf("%s, vous avez ete mange !\nRespawn...\n",joueur[i].nom);
            pause;
        }
        int action_effectuee=FAUX;
        int ok=1;
        while(action_effectuee==FAUX)
        {
            //selection de l'action voulue
            vider_ecran;
            creature[joueur[i].x][joueur[i].y]->selection=2;//on met en avant le joueur en cours
            afficher_carte(creature);//on affiche la carte
            printf("%s, que veux-tu faire ?\n",joueur[i].nom);
            printf("(0) se deplacer\n(1) pecher\n(2) construire un pont (%d unites en stock)\n(3) balancer le dernier poisson peche\n",creature[joueur[i].x][joueur[i].y]->sac);
            if(joueur[i].filet==1)
            {
                printf("(4) pecher avec l'epuisette (une utilisation)\n(5) acheter un filet (une utilisation) (coute 20 unites)\n(6) Sauvegarder la partie en cours\n(7) Ne rien faire\n");
            }else if(joueur[i].filet==2)
            {
                printf("(4) acheter une epuisette (une utilisation)(coute 10 unites)\n(5) pecher avec le filet (une utilisation)\n(6) Sauvegarder la partie en cours\n(7) Ne rien faire\n");
            }else if(joueur[i].filet==3)
            {
                printf("(4) pecher avec l'epuisette (une utilisation)\n(5) pecher avec le filet (une utilisation)\n(6) Sauvegarder la partie en cours\n(7) Ne rien faire\n");
            }else if(joueur[i].filet==0)
            {
                printf("(4) acheter une epuisette (une utilisation)(coute 10 unites)\n(5) acheter un filet (une utilisation) (coute 20 unites)\n(6) Sauvegarder la partie en cours\n(7) Ne rien faire\n");
            }
            do
            {
                printf("Quel est votre choix ? ");
                lire(buffer,99);//on lit ce que l'utilisateur a choisi
            }while(strcmp(buffer,"0")!=0 && strcmp(buffer,"1")!=0 && strcmp(buffer,"2")!=0 && strcmp(buffer,"3")!=0 && strcmp(buffer,"4")!=0 && strcmp(buffer,"5")!=0 && strcmp(buffer,"6")!=0 && strcmp(buffer,"7")!=0 && (strcmp(buffer,"8")!=0));//tant que l'entree n'est pas correcte
            if(strcmp(buffer,"0")==0)//deplacement
            {
                printf("deplacement...\n");
                ok=ISA(0,i,joueur,creature);//on appelle l'ISA
                if(ok)//si l'action s'est bien effectuee
                {
                    action_effectuee=VRAI;//on passe au joueur suivant
                    if(creature[joueur[i].x][joueur[i].y]->contenu_case_actuelle==TERRE)//si le joueur est sur la terre
                    {
                        joueur[i].initX=joueur[i].x;//son point de spawn est mis a jour
                        joueur[i].initY=joueur[i].y;
                    }
                }else//si le joueur a annule
                {
                    action_effectuee=FAUX;//on reste dans la boucle
                }
            }else if(strcmp(buffer,"1")==0)//pecher avec canne a peche
            {
                printf("peche...\n");
                ok=ISA(1,i,joueur,creature);//on appelle l'ISA
                if(ok)//si l'action s'est bien effectuee
                {
                    action_effectuee=VRAI;//on passe au joueur suivant
                }else//si le joueur a annule
                {
                    action_effectuee=FAUX;//on reste dans la boucle
                }
            }else if(strcmp(buffer,"2")==0)//construire un pont
            {
                if(creature[joueur[i].x][joueur[i].y]->sac>0)//si le joueur a quelque chose dans son sac
                {
                    printf("construction...\n");
                    ok=ISA(2,i,joueur,creature);//on appelle l'ISA
                    if(ok)//si l'action s'est bien effectuee
                    {
                        action_effectuee=VRAI;//on passe au joueur suivant
                    }else//si le joueur a annule
                    {
                        action_effectuee=FAUX;//on reste dans la boucle
                    }
                }else//si il n'a rien
                {
                    printf("Vous n'avez rien pour construire !\n");
                    pause;
                    action_effectuee=FAUX;//on reste dans la boucle
                }
            }else if(strcmp(buffer,"3")==0)//balancer un poisson
            {
                if(creature[joueur[i].x][joueur[i].y]->sac>0 && creature[joueur[i].x][joueur[i].y]->derniere_prise!=MER)//si le joueur n'a pas utilise sa derniere prise
                {
                    printf("lancement...\n");
                    ok=ISA(3,i,joueur,creature);//on appelle l'ISA
                    if(ok)//si l'action s'est bien effectuee
                    {
                        action_effectuee=VRAI;//on passe au joueur suivant
                    }else//si le joueur a annule
                    {
                        action_effectuee=FAUX;//on reste dans la boucle
                    }
                }else//si il n'a rien a lancer de vivant
                {
                    printf("Vous n'avez rien de vivant a lancer !\n");
                    pause;
                    action_effectuee=FAUX;//on reste dans la boucle
                }
            }else if(strcmp(buffer,"4")==0)//peche a l'epuisette
            {
                if(joueur[i].filet==1 || joueur[i].filet==3)//si le joueur possede une epuisette
                {
                    printf("peche...\n");
                    ok=ISA(4,i,joueur,creature);//on appelle l'ISA
                    if(ok)//si l'action s'est bien effectuee
                    {
                        action_effectuee=VRAI;//on passe au joueur suivant
                        joueur[i].filet-=1;//on retire l'epuisette du joueur
                    }else//si le joueur a annule
                    {
                        action_effectuee=FAUX;//on reste dans la boucle
                    }
                }else//si le joueur n'a pas d'epuisette
                {
                    if(creature[joueur[i].x][joueur[i].y]->sac>9)//si il a assez d'unites pour en acheter un
                    {
                        creature[joueur[i].x][joueur[i].y]->sac-=10;//il en achete un
                        joueur[i].filet++;//il obtient l'epuisette
                        printf("Achat reussi !\n");
                    }else//si le joueur n'a pas assez d'unites
                    {
                        printf("Vous n'avez pas assez d'unites pour acheter ca !\n");
                        action_effectuee=FAUX;//on reste dans la boucle
                    }
                    pause;
                }
            }else if(strcmp(buffer,"5")==0)//peche au filet
            {
                if(joueur[i].filet==2 || joueur[i].filet==3)//si le joueur a un filet
                {
                    printf("peche...\n");
                    ok=ISA(5,i,joueur,creature);//on appelle l'ISA
                    if(ok)//si l'action s'est bien effectuee
                    {
                        action_effectuee=VRAI;//on passe au joueur suivant
                        joueur[i].filet-=2;//on retire le filet du joueur
                    }else//si le joueur a annule
                    {
                        action_effectuee=FAUX;//on reste dans la boucle
                    }
                }else//si le joueur n'a pas de filet
                {
                    if(creature[joueur[i].x][joueur[i].y]->sac>19)//si il a assez d'unites pour en acheter un
                    {
                        creature[joueur[i].x][joueur[i].y]->sac-=20;//il en achete un
                        joueur[i].filet+=2;//il obtient le filet
                        printf("Achat reussi !\n");
                    }else//si le joueur n'a pas assez d'unites
                    {
                        printf("Vous n'avez pas assez d'unites pour acheter ca !\n");
                        action_effectuee=FAUX;//on reste dans la boucle
                    }
                    pause;
                }
            }else if(strcmp(buffer,"7")==0)//ne rien faire
            {
                ok=ISA(6,i,joueur,creature);//on appelle l'ISA
                if(ok)//si l'action s'est bien effectuee
                {
                    action_effectuee=VRAI;//on passe au joueur suivant
                    creature[joueur[i].x][joueur[i].y]->selection=0;//on deselectionne le joueur
                }
            }else if(strcmp(buffer,"6")==0)//sauvegarde
            {
                fichier=selecteur_slot(1);//on choisit un slot
                if(fichier!=NULL)//si le joueur n'a pas annule
                {
                    enregistrer_partie_en_cours(fichier, tour_courant, joueur, joueurs, creature);//on enregistre la partie en cours
                    printf("\nPartie sauvegardee !\n");
                    pause;
                    action_effectuee=FAUX;//on reste dans la boucle
                }
            }else if(strcmp(joueur[i].nom,"MOISE")==0)//cheat code
            {
                for(j=0;j<M;j++)
                {
                    if(creature[(M-1)/2][j]->type_animal!=PECHEUR)
                    {
                        creer_animal(creature[(M-1)/2][j],TERRE);
                    }
                    if(creature[(M-1)/2+1][j]->type_animal!=PECHEUR)
                    {
                        creer_animal(creature[(M-1)/2+1][j],TERRE);
                    }
                    if(creature[(M-1)/2-1][j]->type_animal!=PECHEUR)
                    {
                        creer_animal(creature[(M-1)/2-1][j],TERRE);
                    }
                }
                action_effectuee=FAUX;
            }
            vider_ecran;
            afficher_carte(creature);//on affiche la carte
            if(joueur[i].Yarrivee==joueur[i].y)//si le joueur est arrive
            {
                pas_gagne=FAUX;//on sort de la boucle principale
            }
        }
        pause;
    }
    free(buffer);
    return pas_gagne;
}

void menu_taille_carte(void)//demande a l'utilisateur la taille de la map
{
    char *buffer=calloc(100,sizeof(char));//buffer de lecture
    char *ptr;
    int var=0;
    printf("\n\nChoisissez la taille de la carte (30-200)\n\n");
    do
    {
        printf("Quel est votre choix ? ");
        lire(buffer,99);//on lit ce que l'utilisateur rentre
        var=strtol(buffer,&ptr,10);//conversion chaine de caractere/entier
    }while(var<30 || var>200);//tant que l'entree n'est pas correcte
    M=var;//on affecte la valeur entree a la taille de la carte
}

void menu_nom_joueurs(int nbjoueurs,pecheur *joueur)//demande aus joueurs leur pseudo
{
    int i,j;
    char *nom=calloc(11,sizeof(char));
    for(i=0;i<nbjoueurs;i++)
    {
        vider_ecran;
        printf("\n\n\nJoueur %i, entrez votre pseudo :\n(10 caracteres max)\n> ",i+1);
        lire(nom,10);
        if(strlen(nom)<1)
        {
            sprintf(nom,"Joueur_%i",i+1);
            strcpy(joueur[i].nom,nom);
        }else
        {
            strcpy(joueur[i].nom,nom);
            for(j=0;j<10;j++)
            {
                if(joueur[i].nom[j]==' ')
                {
                    joueur[i].nom[j]='_';
                }
            }
        }
    }
}
