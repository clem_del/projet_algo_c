#ifndef MENU_H
#define MENU_H
#include "regles.h"
#include "sauvegarder.h"
#include "curseur.h"


int s;//variable d'incrementation speciale pour l'animation de fin
/*********************************************************************************************/
/*Les define qui suivent assurent la compatibilite du programme avec windows, linux et mac os*/
/*********************************************************************************************/
#ifdef __APPLE__//pour les mac os

    #define vider_ecran system("clear")
    #define color(param) printf("\033[%sm","7");printf("%s",param);printf("\033[%sm","0")
    #define hcolor(param) printf("\033[%sm","31");printf("%s",param);printf("\033[%sm","0")
    #define rouge(param) printf("\033[%sm","31");printf("%s",param);printf("\033[%sm","0")
    #define vert(param) printf("\033[%sm","32");printf("%s",param);printf("\033[%sm","0")
    #define anim(param) while(1){for(s=31;s<38;s++){printf("\033[%dm",s);printf("\n\n\n\t\t\t%s a gagne !",param);system("clear");}}printf("\033[%sm","0")
    #define son system("cp ./test.wav /dev/dsp")
    #define resize(param) if(safe==FAUX){printf("\033[8;%d;%dt", param, param);}
    #define touche_entree 10
    #include <termios.h>
    #include <unistd.h>

int mygetch(void);
    #define pause printf("Appuyez sur une touche pour continuer...\n");mygetch()
    #define lire_un() mygetch()
#elif __WIN32//pour les windows
    #include <conio.h>
    #include <windows.h>
    #define touche_entree 13
    #define pause system("pause") //getch()
    #define vider_ecran system("cls")
    #define son system("start /min test.wav")
    char * cmd;
#define resize(param) if(safe==FAUX){cmd=malloc(50*sizeof(char));sprintf(cmd,"mode con lines=%i cols=%i",param,param);printf("%s",cmd);system(cmd);free(cmd);}
    HANDLE hConsole;
    CONSOLE_FONT_INFOEX ConsoleCurrentFontEx;
#define taille_police if(safe==FALSE){hConsole=GetStdHandle(STD_OUTPUT_HANDLE);ConsoleCurrentFontEx.cbSize=sizeof(CONSOLE_FONT_INFOEX);GetCurrentConsoleFontEx(hConsole,0,&ConsoleCurrentFontEx);ConsoleCurrentFontEx.dwFontSize.Y=8;ConsoleCurrentFontEx.dwFontSize.X=8;SetCurrentConsoleFontEx(hConsole,0,&ConsoleCurrentFontEx);}
    #define color(param) hConsole=GetStdHandle(STD_OUTPUT_HANDLE);SetConsoleTextAttribute(hConsole,240);printf("%s",param);SetConsoleTextAttribute(hConsole,7)
    #define hcolor(param) hConsole=GetStdHandle(STD_OUTPUT_HANDLE);SetConsoleTextAttribute(hConsole,12);printf("%s",param);SetConsoleTextAttribute(hConsole,7)
    #define rouge(param) hConsole=GetStdHandle(STD_OUTPUT_HANDLE);SetConsoleTextAttribute(hConsole,12);printf("%s",param);SetConsoleTextAttribute(hConsole,7)
    #define vert(param) hConsole=GetStdHandle(STD_OUTPUT_HANDLE);SetConsoleTextAttribute(hConsole,10);printf("%s",param);SetConsoleTextAttribute(hConsole,7)
    #define lire_un() getch()
    #define anim(param) hConsole=GetStdHandle(STD_OUTPUT_HANDLE);while(1){for(s=1;s<16;s++){SetConsoleTextAttribute(hConsole,s);printf("\n\n\n\t\t\t%s a gagne !",param);system("cls");}}SetConsoleTextAttribute(hConsole,7)
#elif __linux__//pour les linux
    #define vider_ecran system("clear")
    #define color(param) printf("\033[%sm","7");printf("%s",param);printf("\033[%sm","0")
    #define hcolor(param) printf("\033[%sm","31");printf("%s",param);printf("\033[%sm","0")
    #define rouge(param) printf("\033[%sm","31");printf("%s",param);printf("\033[%sm","0")
    #define vert(param) printf("\033[%sm","32");printf("%s",param);printf("\033[%sm","0")
    #define anim(param) while(1){for(s=31;s<38;s++){printf("\033[%dm",s);printf("\n\n\n\t\t\t%s a gagne !",param);system("clear");}}printf("\033[%sm","0")
    #define son system("cp ./test.wav /dev/dsp")
    #define resize(param) if(safe==FAUX){printf("\033[8;%d;%dt", param, param);}
    #define touche_entree 10
    #include <termios.h>
    #include <unistd.h>

int mygetch(void);

    #define pause printf("Appuyez sur une touche pour continuer...\n");mygetch()
    #define lire_un() mygetch()
#endif


void afficher_carte(animal ***creature);//permet d'afficher la carte

int ISA(int mode,int numero_joueur, pecheur *joueur,animal ***creature);//gere la selection sur la carte et les actions du pecheur

char * selecteur_slot(int mode);//selecteur de slots de sauvegarde, renvoie le nom du fichier selectionne

void initialiser_creatures(pecheur *joueur, animal ***creature);//permet de remplir la carte avec des creatures reparties aleatoirement, en respectant les proportions du bestiaire

int menu_mode_de_jeu(pecheur *joueur);//selection du nombre de joueurs

int menu_selection_carte(animal ***creature,int joueurs, pecheur *joueur);//menu de selection de la carte

void menu_sauvegarde_nouvelle_carte(animal ***creature);//menu demandant si l'on veut sauvegarder la carte creee

int menu_action_joueurs(animal ***creature, int joueurs, pecheur *joueur);//menu d'action pour les joueurs, appelle ISA  quand l'action est choisie et gere son retour

void menu_taille_carte(void);//demande a l'utilisateur la taille de la map

void menu_nom_joueurs(int nbjoueurs,pecheur *joueur);//demande au

#endif // MENU_H
