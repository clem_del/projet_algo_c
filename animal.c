#include "animal.h"


void creer_bestiaire(void)//definition des caracteristiques de base des creatures
{
    //bout d'ocean
    bestiaire[MER].nombre_total = 0;
    bestiaire[MER].dernier_repas = 0;
    bestiaire[MER].satiete = 0;
    bestiaire[MER].derniere_reproduction = 0;
    bestiaire[MER].duree_survie =  0;
    bestiaire[MER].taille = 0;
    bestiaire[MER].taille_du_bide = 0;
    bestiaire[MER].saut_max = 0;
    bestiaire[MER].metabolisme = 0;
    bestiaire[MER].gestation = 0;
    bestiaire[MER].frequence_reproduction = 0;
    bestiaire[MER].taux_apparition = (int)(0.10*((M-2)*(M-2)));

    //terre
    bestiaire[TERRE].nombre_total=0;
    bestiaire[TERRE].dernier_repas=0;
    bestiaire[TERRE].satiete=0;
    bestiaire[TERRE].derniere_reproduction=0;
    bestiaire[TERRE].duree_survie=0;
    bestiaire[TERRE].taille=0;
    bestiaire[TERRE].taille_du_bide=0;
    bestiaire[TERRE].saut_max=0;
    bestiaire[TERRE].metabolisme=0;
    bestiaire[TERRE].gestation=0;
    bestiaire[TERRE].frequence_reproduction=0;
    bestiaire[TERRE].taux_apparition = 0;

    //plancton
    bestiaire[PLANCTON].nombre_total=0;
    bestiaire[PLANCTON].dernier_repas=0;
    bestiaire[PLANCTON].satiete=0;
    bestiaire[PLANCTON].derniere_reproduction=0;
    bestiaire[PLANCTON].duree_survie=INFINI;//vie infini
    bestiaire[PLANCTON].taille=1;
    bestiaire[PLANCTON].taille_du_bide=0;
    bestiaire[PLANCTON].saut_max=0;//ne bouge pas
    bestiaire[PLANCTON].metabolisme=0;
    bestiaire[PLANCTON].gestation=0;
    bestiaire[PLANCTON].frequence_reproduction=0;
    bestiaire[PLANCTON].taux_apparition = (int)(0.25*(M-2)*(M-2));

    //corail
    bestiaire[CORAIL].nombre_total=0;
    bestiaire[CORAIL].dernier_repas=0;
    bestiaire[CORAIL].satiete=0;
    bestiaire[CORAIL].derniere_reproduction=0;
    bestiaire[CORAIL].duree_survie=50;
    bestiaire[CORAIL].taille=2;
    bestiaire[CORAIL].taille_du_bide=5;
    bestiaire[CORAIL].saut_max=1;
    bestiaire[CORAIL].metabolisme=1;
    bestiaire[CORAIL].gestation=1;
    bestiaire[CORAIL].frequence_reproduction=5;
    bestiaire[CORAIL].taux_apparition = (int)(0.12*(M-2)*(M-2));

    //bar
    bestiaire[BAR].nombre_total=0;
    bestiaire[BAR].dernier_repas=0;
    bestiaire[BAR].satiete=0;
    bestiaire[BAR].derniere_reproduction=0;
    bestiaire[BAR].duree_survie=10;
    bestiaire[BAR].taille=4;
    bestiaire[BAR].taille_du_bide=10;
    bestiaire[BAR].saut_max=3;
    bestiaire[BAR].metabolisme=1;
    bestiaire[BAR].gestation=2;
    bestiaire[BAR].frequence_reproduction=7;
    bestiaire[BAR].taux_apparition = (int)(0.10*(M-2)*(M-2));

    //thon
    bestiaire[THON].nombre_total=0;
    bestiaire[THON].dernier_repas=0;
    bestiaire[THON].satiete=0;
    bestiaire[THON].derniere_reproduction=0;
    bestiaire[THON].duree_survie=15;
    bestiaire[THON].taille=4;
    bestiaire[THON].taille_du_bide=10;
    bestiaire[THON].saut_max=3;
    bestiaire[THON].metabolisme=1;
    bestiaire[THON].gestation=2;
    bestiaire[THON].frequence_reproduction=10;
    bestiaire[THON].taux_apparition = (int)(0.10*(M-2)*(M-2));

    //pollution
    bestiaire[POLLUTION].nombre_total=0;
    bestiaire[POLLUTION].dernier_repas=0;
    bestiaire[POLLUTION].satiete=5;
    bestiaire[POLLUTION].derniere_reproduction=0;
    bestiaire[POLLUTION].duree_survie=INFINI;//immortel
    bestiaire[POLLUTION].taille=0;
    bestiaire[POLLUTION].taille_du_bide=100;
    bestiaire[POLLUTION].saut_max=1;
    bestiaire[POLLUTION].metabolisme=0;
    bestiaire[POLLUTION].gestation=0;
    bestiaire[POLLUTION].frequence_reproduction=INFINI;//ne se reproduit pas
    bestiaire[POLLUTION].taux_apparition = (int)(0.05*(M-2)*(M-2));

    //pirhanna
    bestiaire[PIRHANNA].nombre_total=0;
    bestiaire[PIRHANNA].dernier_repas=0;
    bestiaire[PIRHANNA].satiete=0;
    bestiaire[PIRHANNA].derniere_reproduction=0;
    bestiaire[PIRHANNA].duree_survie=10;
    bestiaire[PIRHANNA].taille=2;
    bestiaire[PIRHANNA].taille_du_bide=5;
    bestiaire[PIRHANNA].saut_max=2;
    bestiaire[PIRHANNA].metabolisme=1;
    bestiaire[PIRHANNA].gestation=2;
    bestiaire[PIRHANNA].frequence_reproduction=4;
    bestiaire[PIRHANNA].taux_apparition = (int)(0.10*(M-2)*(M-2));

    //requin
    bestiaire[REQUIN].nombre_total=0;
    bestiaire[REQUIN].dernier_repas=0;
    bestiaire[REQUIN].satiete=0;
    bestiaire[REQUIN].derniere_reproduction=0;
    bestiaire[REQUIN].duree_survie=20;
    bestiaire[REQUIN].taille=5;
    bestiaire[REQUIN].taille_du_bide=10;
    bestiaire[REQUIN].saut_max=5;
    bestiaire[REQUIN].metabolisme=1;
    bestiaire[REQUIN].gestation=2;
    bestiaire[REQUIN].frequence_reproduction=10;
    bestiaire[REQUIN].taux_apparition = (int)(0.05*(M-2)*(M-2));

    //orque
    bestiaire[ORQUE].nombre_total=0;
    bestiaire[ORQUE].dernier_repas=0;
    bestiaire[ORQUE].satiete=0;
    bestiaire[ORQUE].derniere_reproduction=0;
    bestiaire[ORQUE].duree_survie=15;
    bestiaire[ORQUE].taille=13;
    bestiaire[ORQUE].taille_du_bide=20;
    bestiaire[ORQUE].saut_max=5;
    bestiaire[ORQUE].metabolisme=2;
    bestiaire[ORQUE].gestation=2;
    bestiaire[ORQUE].frequence_reproduction=8;
    bestiaire[ORQUE].taux_apparition = (int)(0.05*(M-2)*(M-2));

    //baleine
    bestiaire[BALEINE].nombre_total=0;
    bestiaire[BALEINE].dernier_repas=0;
    bestiaire[BALEINE].satiete=5;
    bestiaire[BALEINE].derniere_reproduction=0;
    bestiaire[BALEINE].duree_survie=55;
    bestiaire[BALEINE].taille=100;
    bestiaire[BALEINE].taille_du_bide=100;
    bestiaire[BALEINE].saut_max=5;
    bestiaire[BALEINE].metabolisme=2;
    bestiaire[BALEINE].gestation=3;
    bestiaire[BALEINE].frequence_reproduction=50;
    bestiaire[BALEINE].taux_apparition = (int)(0.08*(M-2)*(M-2));

    //pecheur
    bestiaire[PECHEUR].nombre_total=0;
    bestiaire[PECHEUR].dernier_repas=0;
    bestiaire[PECHEUR].satiete=0;
    bestiaire[PECHEUR].derniere_reproduction=0;
    bestiaire[PECHEUR].duree_survie=0;
    bestiaire[PECHEUR].distance_peche_filet=1;
    bestiaire[PECHEUR].sac=0;
    bestiaire[PECHEUR].taille_canne_a_peche=2;
    bestiaire[PECHEUR].derniere_prise=MER;
    bestiaire[PECHEUR].contenu_case_actuelle=TERRE;
    bestiaire[PECHEUR].frequence_reproduction=0;
    bestiaire[PECHEUR].taux_apparition = 0;

    //pont
    bestiaire[PONT].nombre_total=0;
    bestiaire[PONT].dernier_repas=0;
    bestiaire[PONT].satiete=0;
    bestiaire[PONT].derniere_reproduction=0;
    bestiaire[PONT].duree_survie=0;
    bestiaire[PONT].taille=0;
    bestiaire[PONT].taille_du_bide=0;
    bestiaire[PONT].saut_max=0;
    bestiaire[PONT].metabolisme=0;
    bestiaire[PONT].gestation=0;
    bestiaire[PONT].frequence_reproduction=0;
    bestiaire[PONT].taux_apparition = 0;

    //mur
    bestiaire[MUR].nombre_total=0;
    bestiaire[MUR].dernier_repas=0;
    bestiaire[MUR].satiete=0;
    bestiaire[MUR].derniere_reproduction=0;
    bestiaire[MUR].duree_survie=0;
    bestiaire[MUR].taille=0;
    bestiaire[MUR].taille_du_bide=0;
    bestiaire[MUR].saut_max=0;
    bestiaire[MUR].metabolisme=0;
    bestiaire[MUR].gestation=0;
    bestiaire[MUR].frequence_reproduction=0;
    bestiaire[MUR].taux_apparition = 0;
}

void vider(animal *animal)//remise a 0 de l'animal
{
    animal->type_animal=MER;//on vide tous les attributs de l'animal et on met de la mer
    animal->tour_courant=0;
    animal->dernier_repas=0;
    animal->satiete=0;
    animal->derniere_reproduction=0;
    animal->duree_survie=0;
    animal->taille=0;
    animal->taille_du_bide=0;
    animal->saut_max=0;
    animal->metabolisme=0;
    animal->gestation=0;
    animal->frequence_reproduction=0;
    animal->selection=0;
    animal->filet=0;
}

void creer_animal(animal *animal,int type_espece)//creation d'un animal d'une espece donnee
{
    if(type_espece<=NOMBRE_ESPECES-1)//si l'espece est dans la table
    {
        animal->type_animal=type_espece;//on cree un animal de cette espece avec les caracteristiques de base sauvegardees dans le bestiaire
        animal->tour_courant= tour_courant;//on initialise son tour courant au tour actuel
        animal->dernier_repas=bestiaire[type_espece].dernier_repas;
        animal->satiete=bestiaire[type_espece].taille_du_bide;
        animal->derniere_reproduction=bestiaire[type_espece].derniere_reproduction;
        animal->duree_survie=bestiaire[type_espece].duree_survie;
        animal->taille=bestiaire[type_espece].taille;
        animal->taille_du_bide=bestiaire[type_espece].taille_du_bide;
        animal->saut_max=bestiaire[type_espece].saut_max;
        animal->metabolisme=bestiaire[type_espece].metabolisme;
        animal->gestation=bestiaire[type_espece].gestation;
        animal->frequence_reproduction= bestiaire[type_espece].frequence_reproduction;
        animal->selection=0;
        animal->filet=0;
        bestiaire[type_espece].nombre_total++;//on incremente le nombre total de creatures de cette espece
    }else//sinon, on vide la case
    {
        vider(animal);
    }
}

int est_vide(animal *animal)//retourne 1 si la case est libre (mer)
{
    if(animal->type_animal==MER)//si c'est de la mer
    {
        return VRAI;
    }else//sinon
    {
        return FAUX;
    }
}
