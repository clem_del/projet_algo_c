#ifndef ANIMAL_H
#define ANIMAL_H
#define NOMBRE_ESPECES 15
#define VRAI 1
#define FAUX 0
#define INFINI -1
int M;//taille de la carte
int safe;

//attributs du pecheur (permet de reutiliser la meme structure pour stocker un pecheur et un animal)
#define taille_canne_a_peche saut_max
#define distance_peche_filet taille
#define sac taille_du_bide
#define contenu_case_actuelle gestation
#define derniere_prise metabolisme

int tour_courant;
typedef enum{RIEN,MER,PLANCTON,CORAIL,BAR,THON,POLLUTION,PIRHANNA,REQUIN,ORQUE,BALEINE,PECHEUR,TERRE,PONT,MUR} espece;

typedef struct animal {//structure stockant les attributs de chaque animal
    //variables
    espece type_animal;
    int tour_courant;
    int dernier_repas;
    int satiete;
    int derniere_reproduction;
    //constantes
    int duree_survie;
    int taille;
    int taille_du_bide;
    int saut_max;
    int metabolisme;
    int gestation;
    int frequence_reproduction;
    int selection;
    int filet;
} animal;

typedef struct proprietes_animal {//structure stockant les attributs de base d'une espece
    //variables
    int nombre_total;
    int dernier_repas;
    int satiete;
    int derniere_reproduction;
    //constantes
    int duree_survie;
    int taille;
    int taille_du_bide;
    int saut_max;
    int metabolisme;
    int gestation;
    int frequence_reproduction;
    int taux_apparition;
} proprietes_animal;

proprietes_animal bestiaire[NOMBRE_ESPECES];//tableau rassemblant les caracteristiques de base de toutes les especes existantes

void creer_bestiaire(void);//initialise bestiaire[] avec les valeurs definies par defaut dans le programme

void vider(animal *animal);//vide une case de la map (spawn de la mer)

void creer_animal(animal *animal, int type_espece);//spawn un animal de l'espece type_espece a l'emplacement designe par animal

int est_vide(animal *animal);//retourne vrai si la case contient de la mer

#endif // ANIMAL_H
