all: JDLV

JDLV: ia.o animal.o regles.o sauvegarder.o curseur.o menu.o main.o
	gcc -o JDLV ia.o animal.o regles.o sauvegarder.o curseur.o menu.o main.o
ia.o: ia.c
	gcc -o ia.o -c ia.c -Wall
animal.o: animal.c
	gcc -o animal.o -c animal.c -Wall

curseur.o: curseur.c
	gcc -o curseur.o -c curseur.c -Wall

regles.o: regles.c
	gcc -o regles.o -c regles.c -Wall

sauvegarder.o: sauvegarder.c
	gcc -o sauvegarder.o -c sauvegarder.c -Wall

menu.o: menu.c
	gcc -o menu.o -c menu.c -Wall

main.o: main.c
	gcc -o main.o -c main.c -Wall

clean:
	rm *.o

aclean:
	rm archive*
