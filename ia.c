#include "ia.h"


int zone_la_plus_sure(int *nb_predateur, int *nb_mer)//retourne le numero de la zone ou il y a le moins de risques de se faire manger et ou il y a de la place
{
    int maxi=nb_predateur[0];//nb maxi de predateurs dans une zone
    int zone_la_moins_sure=1;//numero de la zone la moins sure
    int cotes_les_moins_sur[4]={FAUX,FAUX,FAUX,FAUX};//tableau marquant les cotes les moins surs
    int i=0;//compteur
    for(i=1;i<8;i++)//on trouve la zone ou il y a le plus de predateurs
    {
        if(nb_predateur[i]>=maxi)
        {
            maxi=nb_predateur[i];
            zone_la_moins_sure=i+1;
        }
    }

    switch(zone_la_moins_sure)//selon la zone la plus dangereuse
    {
    case 1://si c'est la zone 1
        cotes_les_moins_sur[0]=VRAI;//on marque les cotes ou la zone est presente comme dangereux
        cotes_les_moins_sur[1]=VRAI;
        break;
    case 2://si c'est la zone 2
        cotes_les_moins_sur[1]=VRAI;
        break;
    case 3://si c'est la zone 3
        cotes_les_moins_sur[1]=VRAI;
        cotes_les_moins_sur[2]=VRAI;
        break;
    case 4://si c'est la zone 4
        cotes_les_moins_sur[0]=VRAI;
        cotes_les_moins_sur[3]=VRAI;
        break;
    case 5://si c'est la zone 5
        cotes_les_moins_sur[3]=VRAI;
        break;
    case 6://si c'est la zone 6
        cotes_les_moins_sur[3]=VRAI;
        cotes_les_moins_sur[2]=VRAI;
        break;
    case 7://si c'est la zone 7
        cotes_les_moins_sur[2]=VRAI;
        break;
    case 8://si c'est la zone 8
        cotes_les_moins_sur[0]=VRAI;
        break;
    default:
        break;
    }

    int cote_banni[4]={FAUX,FAUX,FAUX,FAUX};//tableau marquant les cotes bannis
    int tableau_alentours[3][3]={{nb_predateur[3],nb_predateur[7],nb_predateur[0]},//tableau rassemblant le nombre de predateurs dans chaque zone
                                 {nb_predateur[4],000000000000000,nb_predateur[1]},
                                 {nb_predateur[5],nb_predateur[6],nb_predateur[2]}};
    int tableau_mer[3][3]={{nb_mer[3],nb_mer[7],nb_mer[0]},//tableau rassemblant le nombre de places libres dans chaque zone
                           {nb_mer[4],000000000,nb_mer[1]},
                           {nb_mer[5],nb_mer[6],nb_mer[2]}};
    int nb_predateur_de_ce_cote[4]={tableau_alentours[0][0]+tableau_alentours[0][1]+tableau_alentours[0][2],//nombre predateurs en haut
                                    tableau_alentours[0][2]+tableau_alentours[1][2]+tableau_alentours[2][2],//nombre predateurs a droite
                                    tableau_alentours[2][0]+tableau_alentours[2][1]+tableau_alentours[2][2],//nombre predateurs en bas
                                    tableau_alentours[0][0]+tableau_alentours[1][0]+tableau_alentours[2][0]};//nombre predateurs a gauche
    int nb_mer_de_ce_cote[4]={tableau_mer[0][0]+tableau_mer[0][1]+tableau_mer[0][2],//nombre mer en haut
                              tableau_mer[0][2]+tableau_mer[1][2]+tableau_mer[2][2],//nombre mer a droite
                              tableau_mer[2][0]+tableau_mer[2][1]+tableau_mer[2][2],//nombre mer en bas
                              tableau_mer[0][0]+tableau_mer[1][0]+tableau_mer[2][0]};//nombre mer a gauche


    //determination du cote ou il y a de la place le plus sur
    int cote_le_plus_sur=0;
    int minimum=25565;//on trouve le cote ou il y a le moins de predateurs
    for(i=0;i<4;i++)
    {
        if(nb_mer_de_ce_cote[i]>0)//si il y a de la place dans ce cote
        {
            if(nb_predateur_de_ce_cote[i]<minimum && cote_banni[i]==FAUX)//si le cote en cours est plus sur que les precedents et qu'il n'a pas ete banni
            {
                minimum=nb_predateur_de_ce_cote[i];//on sauvegarde le minimum
                cote_le_plus_sur=i;//on prend ce cote comme etant le plus sur
            }
        }else//sinon
        {
            cote_banni[i]=VRAI;//on bannit le cote
        }
    }

    //choix si plusieurs zones conviennent
    int cote_convient[4]={FAUX,FAUX,FAUX,FAUX};
    int nb_cotes_conviennent=0;
    for(i=0;i<4;i++)//on determine combien de cotes conviennent
    {
        if(nb_predateur_de_ce_cote[i]==minimum && cote_banni[i]==FAUX)
        {
            cote_convient[i]=VRAI;
            nb_cotes_conviennent++;
        }
    }
    if(nb_cotes_conviennent==2)//si deux cotes conviennent, on prend le plus sur par rapport aux autres cases
    {
        if(cote_convient[0]==VRAI)//si le cote 0 convient
        {
            if(cote_convient[1]==VRAI)//si le 2eme cote qui convient est le 1
            {
                if(nb_predateur_de_ce_cote[2]>nb_predateur_de_ce_cote[3])//si il y a plus de predateurs dans le cote 2
                {
                    cote_le_plus_sur=0;//on va a l'oppose
                }else//sinon
                {
                    cote_le_plus_sur=1;//on va a l'oppose de l'autre
                }
            }else if(cote_convient[3]==VRAI)//si le 2eme cote qui convient est le 3
            {
                if(nb_predateur_de_ce_cote[2]>nb_predateur_de_ce_cote[1])//si il y a plus de predateurs dans le cote 2
                {
                    cote_le_plus_sur=0;//on va a l'oppose
                }else//sinon
                {
                    cote_le_plus_sur=3;//on va a l'oppose de l'autre
                }
            }else if(cote_convient[2]==VRAI)//si le 2eme cote qui convient est le 2
            {
                if((nb_predateur[5]+nb_predateur[2])>(nb_predateur[3]+nb_predateur[0]))//si il y a plus de predateurs proches du cote 2
                {
                    cote_le_plus_sur=0;//on va a l'oppose
                }else//sinon
                {
                    cote_le_plus_sur=2;//on va dans le cote 2
                }
            }
        }else if(cote_convient[1]==VRAI)//si le cote 1 convient
        {
            if(cote_convient[2]==VRAI)//si le 2eme cote qui convient est le 2
            {
                if(nb_predateur_de_ce_cote[3]>nb_predateur_de_ce_cote[0])//si il y a plus de predateurs dans le cote 3
                {
                    cote_le_plus_sur=1;//on va a l'oppose
                }else//sinon
                {
                    cote_le_plus_sur=2;//on va a l'oppose de l'autre
                }
            }else if(cote_convient[0]==VRAI)//si le 2eme cote qui convient est le 0
            {
                if(nb_predateur_de_ce_cote[3]>nb_predateur_de_ce_cote[2])//si il y a plus de predateurs dans le cote 3
                {
                    cote_le_plus_sur=1;//on va a l'oppose
                }else//sinon
                {
                    cote_le_plus_sur=0;//on va a l'oppose de l'autre
                }
            }else if(cote_convient[3]==VRAI)//si le 2eme cote qui convient est le 3
            {
                if((nb_predateur[3]+nb_predateur[5])>(nb_predateur[0]+nb_predateur[2]))//si il y a plus de predateurs proches du cote 3
                {
                    cote_le_plus_sur=1;//on va a l'oppose
                }else//sinon
                {
                    cote_le_plus_sur=3;//on va dans le cote 3
                }
            }
        }else if(cote_convient[2]==VRAI)//si le cote 2 convient
        {
            if(cote_convient[3]==VRAI)//si le 2eme cote qui convient est le 3
            {
                if(nb_predateur_de_ce_cote[0]>nb_predateur_de_ce_cote[1])//si il y a plus de predateurs dans le cote 0
                {
                    cote_le_plus_sur=2;//on va a l'oppose
                }else//sinon
                {
                    cote_le_plus_sur=3;//on va a l'oppose de l'autre
                }
            }else if(cote_convient[1]==VRAI)//si le 2eme cote qui convient est le 1
            {
                if(nb_predateur_de_ce_cote[3]>nb_predateur_de_ce_cote[0])//si il y a plus de predateurs dans le cote 3
                {
                    cote_le_plus_sur=1;//on va a l'oppose
                }else//sinon
                {
                    cote_le_plus_sur=2;//on va a l'oppose de l'autre
                }
            }else if(cote_convient[0]==VRAI)//si le 2eme cote qui convient est le 0
            {
                if((nb_predateur[5]+nb_predateur[2])>(nb_predateur[3]+nb_predateur[0]))//si il y a plus de predateurs proches du cote 2
                {
                    cote_le_plus_sur=0;//on va a l'oppose
                }else//sinon
                {
                    cote_le_plus_sur=2;//on va dans le cote 2
                }
            }
        }else if(cote_convient[3]==VRAI)//si le cote 3 convient
        {
            if(cote_convient[2]==VRAI)//si le 2eme cote qui convient est le 2
            {
                if(nb_predateur_de_ce_cote[1]>nb_predateur_de_ce_cote[0])//si il y a plus de predateurs dans le cote 3
                {
                    cote_le_plus_sur=3;//on va a l'oppose
                }else//sinon
                {
                    cote_le_plus_sur=2;//on va a l'oppose de l'autre
                }
            }else if(cote_convient[0]==VRAI)//si le 2eme cote qui convient est le 0
            {
                if(nb_predateur_de_ce_cote[1]>nb_predateur_de_ce_cote[2])//si il y a plus de predateurs dans le cote 1
                {
                    cote_le_plus_sur=3;//on va a l'oppose
                }else//sinon
                {
                    cote_le_plus_sur=0;//on va a l'oppose de l'autre
                }
            }else if(cote_convient[1]==VRAI)//si le 2eme cote qui convient est le 1
            {
                if((nb_predateur[3]+nb_predateur[5])>(nb_predateur[0]+nb_predateur[2]))//si il y a plus de predateurs proches du cote 3
                {
                    cote_le_plus_sur=1;//on va a l'oppose
                }else//sinon
                {
                    cote_le_plus_sur=3;//on va dans le cote 3
                }
            }
        }
    }else if(nb_cotes_conviennent==3)//si trois cotes conviennent, on prend celui oppose a celui ou il y a le plus de predateurs
    {
        if(cotes_les_moins_sur[0]==VRAI)
        {
            cote_le_plus_sur=2;
        }else if(cotes_les_moins_sur[1]==VRAI)
        {
            cote_le_plus_sur=3;
        }else if(cotes_les_moins_sur[2]==VRAI)
        {
            cote_le_plus_sur=0;
        }else if(cotes_les_moins_sur[3]==VRAI)
        {
            cote_le_plus_sur=1;
        }
    }else if(nb_cotes_conviennent==4)//si les quatre conviennent, on prend au hasard
    {
        cote_le_plus_sur=(rand()%4);
    }

    //determination de la zone la plus sure dans ce cote
    int zone_du_cote_la_plus_sure=0;
    int choix[3]={0,0,0};
    int alentours[2]={0,0};
    switch(cote_le_plus_sur)//selon le cote le plus sur
    {
    case 0://on met dans le tableau choix le numero des zones contenues dans le cote a utiliser et dans le tableau alentours les zones attenantes a ce cote
        choix[0]=3;
        choix[1]=7;
        choix[2]=0;
        alentours[0]=4;
        alentours[1]=1;
        break;
    case 1:
        choix[0]=0;
        choix[1]=1;
        choix[2]=2;
        alentours[0]=7;
        alentours[1]=6;
        break;
    case 2:
        choix[0]=2;
        choix[1]=6;
        choix[2]=5;
        alentours[0]=1;
        alentours[1]=4;
        break;
    case 3:
        choix[0]=5;
        choix[1]=4;
        choix[2]=3;
        alentours[0]=6;
        alentours[1]=7;
        break;
    default:
        break;
    }
    minimum=nb_predateur[choix[0]];//on trouve la ou les cases ou il y a un minimum de predateurs
    int nb_minimum=0;
    for(i=1;i<3;i++)
    {
        if(nb_predateur[choix[i]]<minimum)
        {
            minimum=nb_predateur[choix[i]];
        }
    }
    for(i=0;i<3;i++)//on compte le nombre de fois qu'il y a ce minimum
    {
        if(nb_predateur[choix[i]]==minimum)
        {
            nb_minimum++;
        }
    }
    if(nb_minimum==1)//si le minimum n'apparait qu'une fois
    {
        for(i=0;i<3;i++)
        {
            if(nb_predateur[choix[i]]==minimum)
            {
                zone_du_cote_la_plus_sure=choix[i];//on designe la zone correspondante comme etant la plus sure
            }
        }
    }else if(nb_minimum==2)//si le minimum apparait deux fois
    {
        if(nb_predateur[choix[0]]==minimum)//si la premiere case au minimum est la premiere
        {
            if(nb_predateur[choix[1]]==minimum)//si la seconde est la deuxieme
            {
                if(nb_predateur[alentours[0]]>nb_predateur[choix[2]])//si il y a plus de predateurs dans la zone a cote de la premiere que dans la zone a cote de la deuxieme
                {
                    zone_du_cote_la_plus_sure=choix[1];//on va dans la deuxieme
                }else//sinon
                {
                    zone_du_cote_la_plus_sure=choix[0];//on va dans la premiere
                }
            }else//si la seconde est la troisieme
            {
                if(nb_predateur[alentours[0]]>nb_predateur[alentours[1]])//si il y a plus de predateurs dans la zone a cote de la premiere que dans la zone a cote de la troisieme
                {
                    zone_du_cote_la_plus_sure=choix[2];//on va dans la troisieme
                }else//sinon
                {
                    zone_du_cote_la_plus_sure=choix[0];//on va dans la premiere
                }
            }
        }else//si la premiere case au minimum est la deuxieme (et donc que la deuxieme est la troisieme)
        {
            if(nb_predateur[choix[0]]>nb_predateur[alentours[1]])//si il y a plus de predateurs dans la zone a cote de la deuxieme que dans la zone a cote de la troisieme
            {
                zone_du_cote_la_plus_sure=choix[2];//on va dans la troisieme
            }else//sinon
            {
                zone_du_cote_la_plus_sure=choix[1];//on va dans la deuxieme
            }
        }
    }else//si toutes les zones sont au minimum
    {
        zone_du_cote_la_plus_sure=rand()%8;//on en prend une au hasard
    }
    return zone_du_cote_la_plus_sure;//on retourne la zone la plus sure
}

direction determinerMeilleurDeplacement(animal ***carte,int i, int j)//determine la direction que doit prendre la creature pour avoir le plus de chance de survivre
{
    int h,k;//coordonnees relatives par rapport a la creature
    int nb_predateur[8];//nombre de predateurs dans chaque zone
    int nb_mer[8];//nombre de cases libres dans chaque zone
    int zone_la_moins_dangereuse;//numero de la zone la moins dangereuse
    direction meilleuredirection=PWD;//direction a prendre
    int nb_total_des_predateurs=0;


    //on decoupe les alentours de la creatures en 8 zones
    // 4 8 1
    // 5 X 2
    // 6 7 3

    int VUE = carte[i][j]->saut_max;//on delimite le champ de vision de la creature
    /* zone haute à droite */
    for(h=i-1;h>=i-VUE;h--)
    {
        for(k=j+1;k<=j+VUE;k++) {
            if(est_dans_carte(h+i,k+j)) {
                nb_predateur[0] += peut_manger(h+i,k+j,i,j,carte);//on sauvegarde le nombre de predateurs qu'il y a dans cette zone
                nb_mer[0] += est_vide(carte[h+i][k+j]);//on sauvegarde le nombre de cases libres qu'il y a dans cette zone
            }
        }
    }

    /* zone du milieu à droite */
    for(h=i-(VUE/2);h<=i+(VUE/2);h++)
    {
        for(k=j+1;k<=j+VUE;k++) {
            if(est_dans_carte(h+i,k+j)) {
                nb_predateur[1] += peut_manger(h+i,k+j,i,j,carte);//on sauvegarde le nombre de predateurs qu'il y a dans cette zone
                nb_mer[1] += est_vide(carte[h+i][k+j]);//on sauvegarde le nombre de cases libres qu'il y a dans cette zone
            }
        }
    }

    /* zone basse à droite */
    for(h=i+1;h<=i+VUE;h++)
    {
        for(k=j+1;k<=j+VUE;k++) {
            if(est_dans_carte(h+i,k+j)) {
                nb_predateur[2] += peut_manger(h+i,k+j,i,j,carte);//on sauvegarde le nombre de predateurs qu'il y a dans cette zone
                nb_mer[2] += est_vide(carte[h+i][k+j]);//on sauvegarde le nombre de cases libres qu'il y a dans cette zone
            }
        }
    }

    /* zone haute à gauche */
    for(h=i-1;h>=i-VUE;h--)
    {
        for(k=j-1;k>=j-VUE;k--) {
            if(est_dans_carte(h+i,k+j)) {
                nb_predateur[3] += peut_manger(h+i,k+j,i,j,carte);//on sauvegarde le nombre de predateurs qu'il y a dans cette zone
                nb_mer[3] += est_vide(carte[h+i][k+j]);//on sauvegarde le nombre de cases libres qu'il y a dans cette zone
            }
        }
    }

    /* zone du milieu à gauche */
    for(h=i-(VUE/2);h<=i+(VUE/2);h++)
    {
        for(k=j-1;k>=j-VUE;k--) {
            if(est_dans_carte(h+i,k+j)) {
                nb_predateur[4] += peut_manger(h+i,k+j,i,j,carte);//on sauvegarde le nombre de predateurs qu'il y a dans cette zone
                nb_mer[4] += est_vide(carte[h+i][k+j]);//on sauvegarde le nombre de cases libres qu'il y a dans cette zone
            }
        }
    }

    /* zone basse à gauche */
    for(h=i+1;h<=i+VUE;h++)
    {
        for(k=j-1;k>=j-VUE;k--) {
            if(est_dans_carte(h+i,k+j)) {
                nb_predateur[5] += peut_manger(h+i,k+j,i,j,carte);//on sauvegarde le nombre de predateurs qu'il y a dans cette zone
                nb_mer[5] += est_vide(carte[h+i][k+j]);//on sauvegarde le nombre de cases libres qu'il y a dans cette zone
            }
        }
    }


    /* zone centre bas */
    for(h=i+1;h<=i+VUE;h++)
    {
        for(k=j+VUE/2;k>=j-VUE/2;k--)
        {
            if(est_dans_carte(h+i,k+j)) {
                nb_predateur[6] += peut_manger(h+i,k+j,i,j,carte);//on sauvegarde le nombre de predateurs qu'il y a dans cette zone
                nb_mer[6] += est_vide(carte[h+i][k+j]);//on sauvegarde le nombre de cases libres qu'il y a dans cette zone
            }
        }
    }

    /* zone milieu en haut */
    for(h=i-1;h>=i-VUE;h--)
    {
        for(k=j+VUE/2;k>=j-VUE/2;k--)
        {
            if(est_dans_carte(h+i,k+j)) {
                nb_predateur[7] += peut_manger(h+i,k+j,i,j,carte);//on sauvegarde le nombre de predateurs qu'il y a dans cette zone
                nb_mer[7] += est_vide(carte[h+i][k+j]);//on sauvegarde le nombre de cases libres qu'il y a dans cette zone
            }
        }
    }

    //on compte le nombre total de predateurs dans les environs de la creature
    for(h=0;h<8;h++)
    {
        nb_total_des_predateurs+=nb_predateur[h];
    }


    //determination meilleure zone
    if(nb_total_des_predateurs==0)//si il n'y a pas de predateurs dans les environs
    {
        zone_la_moins_dangereuse=((rand()%8)+1);//on prend une direction au hasard
    }else//si il y a au moins un prédateur
    {
        zone_la_moins_dangereuse=zone_la_plus_sure(nb_predateur,nb_mer);//on prend la plus sûre
        zone_la_moins_dangereuse++;
    }

    switch(zone_la_moins_dangereuse)//selon l'emplacement de la zone la moins dangereuse
    {
    case 1:
        meilleuredirection=DIAGONALE_HAUT_DROITE;//on choisit la direction associee
        break;
    case 2:
        meilleuredirection=DROITE;
        break;
    case 3:
        meilleuredirection=DIAGONALE_BAS_DROITE;
        break;
    case 4:
        meilleuredirection=DIAGONALE_HAUT_GAUCHE;
        break;
    case 5:
        meilleuredirection=GAUCHE;
        break;
    case 6:
        meilleuredirection=DIAGONALE_BAS_GAUCHE;
        break;
    case 7:
        meilleuredirection=BAS;
        break;
    case 8:
        meilleuredirection=HAUT;
        break;
    default:
        meilleuredirection=PWD;
        break;
    }
    return meilleuredirection;//on retourne la direction choisie
}
